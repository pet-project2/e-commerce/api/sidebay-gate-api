﻿using System.ComponentModel;

namespace SideBay.Gate.Utilities.Enumes
{
    public enum ErrorType
    {
        [Description("Model state is invalid")]
        MODEL_INVALID = 0,

        [Description("Internal Server Error, Something is happened")]
        INTERNAL_SERVER_ERROR = 1,

        [Description("Data is existed")]
        EXISTED = 2,

        [Description("Data created failed")]
        CREATED = 3
    }
}
