﻿using System.ComponentModel;

namespace SideBay.Gate.Utilities.Enumes
{
    public enum EventTypes
    {
        [Description("Event's type is created")]
        CREATE = 0,

        [Description("Event's type is updated")]
        UPDATE = 1,

        [Description("Event's type is deleted")]
        DELETE = 2,
    }
}
