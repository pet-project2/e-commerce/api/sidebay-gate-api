﻿using System;
using System.Linq;
using System.Reflection;

namespace SideBay.Gate.Utilities.Extensions
{
    public static class AppDomainExtensions
    {
        /// <summary>
        /// https://stackoverflow.com/questions/1913057/how-to-get-c-net-assembly-by-name
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Assembly GetAssemblyByName(this AppDomain domain, string name)
        {
            return domain.GetAssemblies().FirstOrDefault(x => x.GetName().Name.Contains(name));
        }
    }
}
