﻿using SideBay.Gate.Core.Events;

namespace SideBay.Gate.Core.IntegrationEvents
{
    public class IntegrationEvent<T> : IEvent where T : IEvent
    {
        public Head Head { get; set; }

        public Body<T> Body { get; set; }
    }
}
