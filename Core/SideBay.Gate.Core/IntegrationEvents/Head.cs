﻿using System;

namespace SideBay.Gate.Core.IntegrationEvents
{
    public class Head
    {
        public string EventId { get; private set; } = Guid.NewGuid().ToString();
        public DateTimeOffset EventDate { get; private set; } = DateTimeOffset.UtcNow;
        public string EventType { get; set; }
        public string EventName { get; set; }
    }
}
