﻿namespace SideBay.Gate.Core.IntegrationEvents
{
    public class Body<T>
    {
        T Data { get; set; }
    }
}
