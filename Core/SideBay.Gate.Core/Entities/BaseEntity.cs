﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Entities
{
    public abstract class BaseEntity<TId>
    {
        public TId Id { get; protected set; }
        private List<INotification> _domainEvents;
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents.AsReadOnly();

        public BaseEntity(TId Id)
        {
            this.Id = Id;
            this._domainEvents = new List<INotification>();
        }

        public void AddDomainEvent(INotification @event)
        {
            this._domainEvents?.Add(@event);
        }

        public void RemoveDomainEvent(INotification @event)
        {
            this._domainEvents?.Remove(@event);
        }

        public void ClearDomainEvents()
        {
            this._domainEvents?.Clear();
        }
    }
}
