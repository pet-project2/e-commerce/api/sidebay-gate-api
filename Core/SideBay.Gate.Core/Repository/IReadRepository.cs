﻿using SideBay.Gate.Core.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Repository
{
    public interface IReadRepository<T> where T : class, IAggregateRoot
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(string id);
    }
}
