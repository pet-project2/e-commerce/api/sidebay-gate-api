﻿using SideBay.Gate.Core.Interface;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Repository
{
    public interface IWriteRepository<T> where T : class, IAggregateRoot
    {
        Task AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task DeleteAsync(string id);
    }
}
