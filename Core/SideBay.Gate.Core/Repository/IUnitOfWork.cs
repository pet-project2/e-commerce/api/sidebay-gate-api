﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Models;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Repository
{
    public interface IUnitOfWork<T> where T : class, IAggregateRoot
    {
        Task<bool> Commit();
        Task Dispatch(IBaseModel model);

    }
}
