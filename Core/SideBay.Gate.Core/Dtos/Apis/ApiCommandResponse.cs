﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Dtos.Apis
{
    public class ApiCommandResponse : ApiBaseResponse
    {
        public ApiCommandResponse(string message, int statusCode)
            : base(true, message, statusCode)
        {

        }
    }
}
