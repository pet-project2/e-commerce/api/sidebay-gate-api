﻿using SideBay.Gate.Utilities.Enumes;
using System.Text.Json.Serialization;

namespace SideBay.Gate.Core.Dtos.Apis
{
    public class Error<T>
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public ErrorType Type { get; private set; }
        public T Reason { get; private set; }

        public Error(ErrorType errorType, T reason)
        {
            this.Type = errorType;
            this.Reason = reason;
        }
    }
}
