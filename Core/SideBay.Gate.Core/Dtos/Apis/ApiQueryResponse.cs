﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Dtos.Apis
{
    public class ApiQueryResponse<T> : ApiBaseResponse
    {
        public T Data { get; private set; }

        public ApiQueryResponse(string message, int statusCode, T data) : base(true, message, statusCode)
        {
            this.Data = data;
        }
    }
}
