﻿using SideBay.Gate.Utilities.Enumes;

namespace SideBay.Gate.Core.Dtos.Applications
{
    public class Failure
    {
        public ErrorType ErrorType { get; private set; }
        public string Reason { get; private set; }
        public Failure(ErrorType errorType, string reason)
        {
            this.ErrorType = errorType;
            this.Reason = reason;
        }
    }
}
