﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Core.Dtos.Applications
{
    public class ListDto<T>
    {
        public IEnumerable<T> Products { get; set; }
        public Paging Paging { get; set; }
    }
}
