﻿using System;

namespace SideBay.Gate.Core.Events
{
    public abstract class BaseEvent<TId, T> : IEvent
    {
        public T Data { get; private set; }
        public TId EventId { get; private set; }
        public abstract string EventType { get; }
        public abstract string EventName { get; }
        public DateTimeOffset DateOccurred { get; protected set; } = DateTimeOffset.UtcNow;
        public BaseEvent(TId id)
        {
            this.EventId = id;
        }

        public virtual void SetData(T data)
        {
            this.Data = data;
        }
    }
}
