﻿using System.Threading.Tasks;

namespace SideBay.Gate.Core.Events
{
    public interface IEventHandler<T> where T : IEvent
    {
        Task Publish(T messageEvent);
    }
}
