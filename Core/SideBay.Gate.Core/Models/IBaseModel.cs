﻿using MediatR;
using System.Collections.Generic;

namespace SideBay.Gate.Core.Models
{
    public interface IBaseModel
    {
        public IReadOnlyCollection<INotification> DomainEvents { get; set; }
    }
}
