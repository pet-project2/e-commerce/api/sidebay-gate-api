using FluentAssertions;
using SideBay.Gate.Service.Product.Domain.Aggregate;
using SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects;
using System;
using System.Collections.Generic;
using Xunit;

namespace SideBay.Gate.Tests.Product.Domain
{
    public class ProductAggregate_Spec
    {
        [Fact]
        public void Should_Create_Product_Aggregate_Object_With_Normal_Value()
        {
            var id = Guid.NewGuid();
            var name = "Laptop Asus";
            var description = "Whether at work or play, ASUS VivoBook 15 is the compact";
            var summary = "For warranty information about this product";
            var price = new Money(39.99m, "VND");
            var discount = 10m;

            var product = new ProductAggregate(id, name, description, summary, price, discount);

            product.Should().NotBeNull();
            product.Id.Should().NotBeEmpty();
            product.Name.Should().Be(name);
            product.Description.Should().Be(description);
            product.Summary.Should().Be(summary);
            product.Price.Should().Be(price);
            product.Discount.Should().Be(discount);

            var actualPrice = price.Amount - price.Amount * (discount / 100);
            product.ActualPrice.Amount.Should().Be(actualPrice);
            product.ActualPrice.Currency.CurrencyCode.Should().Be(price.Currency.CurrencyCode);
        }


        [Fact]
        public void Should_Create_Product_Aggregate_With_Gallary_Image()
        {
            var id = Guid.NewGuid();
            var name = "Laptop Asus";
            var description = "Whether at work or play, ASUS VivoBook 15 is the compact";
            var summary = "For warranty information about this product";
            var price = new Money(39.99m, "VND");
            var discount = 10m;
            IEnumerable<string> gallary = new[]
            {
                    "~/image1.png",
                    "~/image2.png",
                };

            var product = new ProductAggregate(id, name, description, summary, price, discount);
            product.AddGallery(gallary);

            product.Should().NotBeNull();
            product.Gallery.Should().NotBeEmpty();
            product.Gallery.Should().HaveSameCount(gallary);
        }
    }
}
