﻿using SideBay.Gate.Service.Product.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Tests.Product.Infrastructure.Helper
{
    public static class EfDbContextHelper
    {
        public static void SeedProductData(this AppDbContext context)
        {
            var products = SampleProductData();
            context.Products.AddRange(products);
            context.SaveChanges();
        }

        public static List<Service.Product.Infrastructure.Models.Product> SampleProductData()
        {
            return new List<Service.Product.Infrastructure.Models.Product>()
            {
                new Service.Product.Infrastructure.Models.Product()
                {
                    Id = Guid.NewGuid(),
                    Name = "Laptop Asus",
                    CreatedAt = DateTime.Now,
                    Description = "Laptop Asus G53 Rog Trix",
                    Discount = 10m,
                    Price = 150m,
                    PublishedAt = DateTime.Now,
                    Summary = "Laptop Asus Series with Intel 10th, 128GB SSD",
                    UpdatedAt = DateTime.Now,
                    Gallery = new List<Service.Product.Infrastructure.Models.Image>()
                    {
                        new Service.Product.Infrastructure.Models.Image
                        {
                            CreatedAt = DateTime.Now,
                            Id = Guid.NewGuid(),
                            IsMainAvatar = true,
                            Path = "~/path1.png",
                        },
                        new Service.Product.Infrastructure.Models.Image
                        {
                            CreatedAt = DateTime.Now,
                            Id = Guid.NewGuid(),
                            IsMainAvatar = true,
                            Path = "~/path2.png",
                        }
                    }
                },
                new Service.Product.Infrastructure.Models.Product()
                {
                    Id = Guid.NewGuid(),
                    Name = "Mainboard Asus",
                    CreatedAt = DateTime.Now,
                    Description = "Mainboard Asus G53 Rog Trix",
                    Discount = 10m,
                    Price = 150m,
                    PublishedAt = DateTime.Now,
                    Summary = "Mainboard Asus Series ROG TRIX",
                    UpdatedAt = DateTime.Now,
                    Gallery = new List<Service.Product.Infrastructure.Models.Image>()
                    {
                        new Service.Product.Infrastructure.Models.Image
                        {
                            CreatedAt = DateTime.Now,
                            Id = Guid.NewGuid(),
                            IsMainAvatar = true,
                            Path = "~/path1.png",
                        },
                        new Service.Product.Infrastructure.Models.Image
                        {
                            CreatedAt = DateTime.Now,
                            Id = Guid.NewGuid(),
                            IsMainAvatar = true,
                            Path = "~/path2.png",
                        }
                    }
                }
            };
        }
    }
}
