﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using SideBay.Gate.Service.Product.Infrastructure;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ProductRepositoryEf;
using SideBay.Gate.Tests.Product.Infrastructure.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SideBay.Gate.Tests.Product.Infrastructure.RepositoryTest.EfRepositoryTest.ProductEfRepositoryTests
{
    public class ProductEfRepository_Spec : IDisposable
    {
        private readonly AppDbContext _appDbContext;
        private readonly ProductEfRepository _productEfRepository;
        private readonly EfUnitOfWork _unitOfWork;

        public ProductEfRepository_Spec()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                            .UseInMemoryDatabase(Guid.NewGuid().ToString())
                            .Options;
            this._appDbContext = new AppDbContext(options);
            this._productEfRepository = new ProductEfRepository(this._appDbContext);

            var logger = Mock.Of<ILogger<EfUnitOfWork>>();

            this._unitOfWork = new EfUnitOfWork(_appDbContext, logger);
        }

        
        [Fact]
        public async Task Should_AddProductAsync_Add_Product_To_Database_Successful()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var product = new Service.Product.Infrastructure.Models.Product()
            {
                Id = Guid.NewGuid(),
                Name = "Router Asus",
                CreatedAt = DateTime.Now,
                Description = "Router Asus G53 Rog Trix",
                Discount = 5m,
                Price = 300m,
                PublishedAt = DateTime.Now,
                Summary = "Router Asus Series with Intel 10th, 128GB SSD",
                UpdatedAt = DateTime.Now,
                Gallery = new List<Service.Product.Infrastructure.Models.Image>()
                {
                    new Service.Product.Infrastructure.Models.Image
                    {
                        CreatedAt = DateTime.Now,
                        Id = Guid.NewGuid(),
                        IsMainAvatar = true,
                        Path = "~/path1.png",
                    },
                    new Service.Product.Infrastructure.Models.Image
                    {
                        CreatedAt = DateTime.Now,
                        Id = Guid.NewGuid(),
                        IsMainAvatar = true,
                        Path = "~/path2.png",
                    }
                }
            };

            await this._productEfRepository.AddProductAsync(product);
            await this._unitOfWork.Commit();

            var products = await this._appDbContext.Products.ToListAsync();
            products.Should().HaveCount(3);

        }

        [Fact]
        public async Task Should_UpdateProduct_Update_Product_To_Database_Successful()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var product = await this._appDbContext.Products.FirstOrDefaultAsync();
            product.Name = "Networking Route";

            this._productEfRepository.UpdateProduct(product);
            await this._unitOfWork.Commit();

            var actual = await this._productEfRepository.GetByProductIdAsync(product.Id);
            actual.Name.Should().Be(product.Name);
        }

        [Fact]
        public async Task Should_UpdateProduct_ByNewInstance_Update_Product_To_Database_Successful()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var product = await this._appDbContext.Products.FirstOrDefaultAsync();
            var newProduct = new Service.Product.Infrastructure.Models.Product()
            {
                Id = product.Id,
                Name = "Router Asus",
                CreatedAt = DateTime.Now,
                Description = "Router Asus G53 Rog Trix",
                Discount = 5m,
                Price = 300m,
                PublishedAt = DateTime.Now,
                Summary = "Router Asus Series with Intel 10th, 128GB SSD",
                UpdatedAt = DateTime.Now,
                Gallery = new List<Service.Product.Infrastructure.Models.Image>()
                {
                    new Service.Product.Infrastructure.Models.Image
                    {
                        CreatedAt = DateTime.Now,
                        Id = Guid.NewGuid(),
                        IsMainAvatar = true,
                        Path = "~/path1.png",
                    }
                }
            };

            this._productEfRepository.UpdateProduct(product, newProduct);
            await this._unitOfWork.Commit();

            var actual = await this._productEfRepository.GetByProductIdAsync(product.Id);
            actual.Should().NotBeNull();
            actual.Should().BeEquivalentTo(newProduct);
        }

        [Fact]
        public async Task Should_Delete_Product_By_Id_To_Database_Successfully()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var product = this._appDbContext.Products.FirstOrDefault();
            await this._productEfRepository.DeleteProductAsync(product.Id);
            await this._unitOfWork.Commit();

            var products = await this._productEfRepository.GetListProductAsync(0, 10);
            products.Should().HaveCount(1);
        }


        [Fact]
        public async Task Should_Delete_Product_To_Database_Successfully()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var product = this._appDbContext.Products.FirstOrDefault();
            this._productEfRepository.DeleteProductAsync(product);
            await this._unitOfWork.Commit();

            var products = await this._productEfRepository.GetListProductAsync(0, 10);
            products.Should().HaveCount(1);
        }

        [Fact]
        public async Task Should_Get_Product_By_Id_From_Database_Successfully()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var product = this._appDbContext.Products.FirstOrDefault();
            var productId = product.Id;

            var actual = await this._productEfRepository.GetByProductIdAsync(productId);
            actual.Should().BeEquivalentTo(product);
        }

        [Fact]
        public async Task Should_Get_Products_From_Database_Successfully()
        {
            this._appDbContext.Database.EnsureCreated();
            this._appDbContext.SeedProductData();

            var actual = await this._productEfRepository.GetListProductAsync(0, 10);
            actual.Should().HaveCount(2);
        }

        public void Dispose()
        {
            this._appDbContext.Database.EnsureDeleted();
        }
    }
}
