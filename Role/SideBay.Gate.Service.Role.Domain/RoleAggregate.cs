﻿using SideBay.Gate.Core.Entities;
using SideBay.Gate.Core.Interface;

namespace SideBay.Gate.Service.Role.Domain
{
    public class RoleAggregate : Entity<Guid>, IAggregateRoot
    {
        public string Name { get; private set; }
        public DateTime CreatedDate { get; private set; }

        public RoleAggregate(Guid id) : base(id) { }
        public RoleAggregate(Guid id, string name) : this(id)
        {
            this.Name = name;
        }
    }
}
