﻿using MediatR;
using SideBay.Gate.Core.Dtos.Applications;
using SideBay.Gate.Service.Role.Infrastructure.Repository;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;

namespace SideBay.Gate.Service.Role.Application.Command.Create
{
    public class AccountRoleCreateCommandHandler : IRequestHandler<AccountRoleCreateCommand, IResult<Unit>>
    {
        private readonly IRoleUnitOfWork<AppRole> _unitOfWork;

        public AccountRoleCreateCommandHandler(IRoleUnitOfWork<AppRole> unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public Task<IResult<Unit>> Handle(AccountRoleCreateCommand request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
