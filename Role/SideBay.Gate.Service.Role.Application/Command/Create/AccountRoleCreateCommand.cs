﻿using MediatR;
using SideBay.Gate.Core.Dtos.Applications;

namespace SideBay.Gate.Service.Role.Application.Command.Create
{
    public class AccountRoleCreateCommand : IRequest<IResult<Unit>>
    {
        public string Name { get; set; }
    }
}
