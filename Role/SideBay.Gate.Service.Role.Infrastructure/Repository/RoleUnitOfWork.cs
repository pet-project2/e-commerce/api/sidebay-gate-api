﻿using MediatR;
using SideBay.Gate.Core.Models;
using SideBay.Gate.Shared.Service.Authen.Infrastructure;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Role.Infrastructure.Repository
{
    public class RoleUnitOfWork : IRoleUnitOfWork<AppRole>
    {
        public IRoleReadRepository<AppRole> RoleReadRepository { get; }
        public IRoleWriteRepository<AppRole> RoleWriteRepository { get; }
        private readonly AppDbContext _dbContext;
        private readonly IMediator _mediator;

        public RoleUnitOfWork(IRoleReadRepository<AppRole> roleReadRepository,
            IRoleWriteRepository<AppRole> roleWriteRepository,
            AppDbContext dbContext, IMediator mediator)
        {
            this.RoleReadRepository = roleReadRepository;
            this.RoleWriteRepository = roleWriteRepository;
            this._dbContext = dbContext;
            this._mediator = mediator;
        }

        public async Task<bool> Commit()
        {
            var result = await _dbContext.SaveChangesAsync();
            return result > 0;
        }

        public Task Dispatch(IBaseModel model)
        {
            throw new NotImplementedException();
        }
    }
}
