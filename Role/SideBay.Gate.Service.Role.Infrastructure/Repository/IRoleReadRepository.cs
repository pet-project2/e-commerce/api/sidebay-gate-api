﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Repository;

namespace SideBay.Gate.Service.Role.Infrastructure.Repository
{
    public interface IRoleReadRepository<T> : IReadRepository<T> where T : class, IAggregateRoot
    {

    }
}
