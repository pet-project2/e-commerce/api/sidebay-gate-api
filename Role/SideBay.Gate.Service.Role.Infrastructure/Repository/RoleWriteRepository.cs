﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Constants;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;
using SideBay.Gate.Utilities.Extensions;
using System.Security.Claims;

namespace SideBay.Gate.Service.Role.Infrastructure.Repository
{
    public class RoleWriteRepository : IRoleWriteRepository<AppRole>
    {
        private readonly RoleManager<AppRole> _roleManager;
        private ILogger<RoleWriteRepository> _logger;

        public RoleWriteRepository(RoleManager<AppRole> roleManager, ILogger<RoleWriteRepository> logger)
        {
            _roleManager = roleManager;
            _logger = logger;
        }

        public Task AddAsync(AppRole entity)
        {
            throw new NotImplementedException();
        }

        public async Task AddPersmissionClaim(string permission, AppRole role)
        {
            var type = PermissionType.PERMISSION.AsText();
            await this._roleManager.AddClaimAsync(role, new Claim(type, permission));
        }

        public async Task AddPersmissionClaims(IEnumerable<string> permissions, AppRole role)
        {
            foreach (var permission in permissions)
            {
                await this.AddPersmissionClaim(permission, role);
            }
        }

        public async Task<bool> CreateRoleAsync(AppRole role)
        {
            var result = await _roleManager.CreateAsync(role);
            if (!result.Succeeded)
            {
                LogErr(result.Errors);
            }

            return result.Succeeded;
        }

        public Task DeleteAsync(AppRole entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(AppRole entity)
        {
            throw new NotImplementedException();
        }

        private void LogErr(IEnumerable<IdentityError> errors)
        {
            foreach (var error in errors)
            {
                _logger.LogError(error.Description);
            }
        }
    }
}
