﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Repository;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;

namespace SideBay.Gate.Service.Role.Infrastructure.Repository
{
    public interface IRoleWriteRepository<T> : IWriteRepository<T> where T : class, IAggregateRoot
    {
        public Task<bool> CreateRoleAsync(AppRole role);

        public Task AddPersmissionClaims(IEnumerable<string> permissions, AppRole role);
        public Task AddPersmissionClaim(string permission, AppRole role);
    }
}
