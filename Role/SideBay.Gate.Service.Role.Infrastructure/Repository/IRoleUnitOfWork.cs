﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Repository;

namespace SideBay.Gate.Service.Role.Infrastructure.Repository
{
    public interface IRoleUnitOfWork<T> : IUnitOfWork<T> where T : class, IAggregateRoot
    {
        public IRoleReadRepository<T> RoleReadRepository { get; }
        public IRoleWriteRepository<T> RoleWriteRepository { get; }
    }
}
