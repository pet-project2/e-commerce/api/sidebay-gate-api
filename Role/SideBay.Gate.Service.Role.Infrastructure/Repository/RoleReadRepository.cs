﻿using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;

namespace SideBay.Gate.Service.Role.Infrastructure.Repository
{
    public class RoleReadRepository : IRoleReadRepository<AppRole>
    {
        public Task<IEnumerable<AppRole>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<AppRole> GetByIdAsync(string id)
        {
            throw new NotImplementedException();
        }
    }
}
