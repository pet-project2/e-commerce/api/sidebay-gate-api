﻿namespace SideBay.Gate.Service.Account.Utilities.Constants
{
    public static class Message
    {
        public static class Account
        {
            public const string ACCOUNT_ERROR_EXISTED = "Account is existed";
            public const string ACCOUNT_ERROR_CREATED = "Account can not created";
            public const string ACCOUNT_CREATED_SUUCESS = "Account is created successfully";

            public static class Gender
            {
                public const string MALE = "Male";
                public const string FEMALE = "Female";
            }
        }
    }
}
