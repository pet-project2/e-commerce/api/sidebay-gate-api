﻿using SideBay.Gate.Core.Entities;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Account.Domain.ValueObjects
{
    public class AccountUserName : ValueObject
    {
        private string _value { get; }

        internal AccountUserName(string userName) => _value = userName;

        public static implicit operator string(AccountUserName userName) => userName._value;

        public static implicit operator AccountUserName(Email email) => new AccountUserName(email.ToString());

        public static AccountUserName FromString(string userName) => new AccountUserName(userName);

        public static AccountUserName FromEmail(Email email) => new AccountUserName(email.ToString());

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }
        public override string ToString()
        {
            return _value;
        }
    }
}
