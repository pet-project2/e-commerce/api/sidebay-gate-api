﻿using SideBay.Gate.Core.Entities;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Account.Domain.ValueObjects
{
    public class PhoneNumber : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(PhoneNumber phoneNumber) => phoneNumber._value;

        internal PhoneNumber(string phoneNumber) => this._value = phoneNumber;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static PhoneNumber FromString(string phoneNumber) => new PhoneNumber(phoneNumber);

        public override string ToString()
        {
            return this._value;
        }
    }
}
