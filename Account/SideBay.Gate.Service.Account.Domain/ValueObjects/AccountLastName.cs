﻿using SideBay.Gate.Core.Entities;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Account.Domain.ValueObjects
{
    public class AccountLastName : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(AccountLastName userLastName) => userLastName._value;

        internal AccountLastName(string userLastName) => this._value = userLastName;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static AccountLastName FromString(string userLastName) => new AccountLastName(userLastName);

        public override string ToString()
        {
            return this._value;
        }
    }
}
