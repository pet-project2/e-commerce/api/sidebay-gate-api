﻿using SideBay.Gate.Core.Entities;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Account.Domain.ValueObjects
{
    public class AccountPassword : ValueObject
    {
        private string _value { get; }

        internal AccountPassword(string password) => _value = password;
        public static implicit operator string(AccountPassword password) => password._value;
        public static AccountPassword FromString(string userName) => new AccountPassword(userName);
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}
