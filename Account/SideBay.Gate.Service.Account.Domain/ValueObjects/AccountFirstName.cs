﻿using SideBay.Gate.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Domain.ValueObjects
{
    public class AccountFirstName : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(AccountFirstName userFirstName) => userFirstName._value;

        internal AccountFirstName(string userFirstName) => this._value = userFirstName;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static AccountFirstName FromString(string userFirstName) => new AccountFirstName(userFirstName);

        public override string ToString()
        {
            return this._value;
        }
    }
}
