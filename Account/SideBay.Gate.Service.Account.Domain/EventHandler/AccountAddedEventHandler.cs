﻿using MediatR;
using Serilog;
using SideBay.Gate.Core.Events;
using SideBay.Gate.Core.IntegrationEvents;
using SideBay.Gate.Service.Account.Domain.Events;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Domain.EventHandler
{
    public class AccountAddedEventHandler : INotificationHandler<AccountAddedEvent>
    {
        private readonly IEventHandler<IntegrationEvent<AccountAddedEvent>> _messagePublisher;
        private readonly ILogger _logger;

        public AccountAddedEventHandler(IEventHandler<IntegrationEvent<AccountAddedEvent>> messagePublisher, ILogger logger)
        {
            _messagePublisher = messagePublisher;
            _logger = logger;
        }

        public Task Handle(AccountAddedEvent notification, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
