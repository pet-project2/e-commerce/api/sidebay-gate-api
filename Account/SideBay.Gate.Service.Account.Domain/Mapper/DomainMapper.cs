﻿using AutoMapper;
using System;

namespace SideBay.Gate.Service.Account.Domain.Mapper
{
    public class DomainMapper
    {
        public static IMapper Mapper => _lazy.Value;
        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {

            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
