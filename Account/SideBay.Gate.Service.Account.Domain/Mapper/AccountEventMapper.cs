﻿using AutoMapper;
using SideBay.Gate.Core.IntegrationEvents;
using SideBay.Gate.Service.Account.Domain.Events;

namespace SideBay.Gate.Service.Account.Domain.Mapper
{
    public class AccountEventMapper : Profile
    {
        public AccountEventMapper()
        {
            CreateMap<AccountAddedEvent, IntegrationEvent<AccountAddedEvent>>()
                .ForPath(d => d.Head.EventId, opt => opt.MapFrom(src => src.EventId))
                .ForPath(d => d.Head.EventId, opt => opt.MapFrom(src => src.EventId));
        }
    }
}
