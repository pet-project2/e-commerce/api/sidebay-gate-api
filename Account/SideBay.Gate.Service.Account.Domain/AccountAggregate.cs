﻿using SideBay.Gate.Core.Entities;
using SideBay.Gate.Core.Interface;
using SideBay.Gate.Service.Account.Domain.Events;
using SideBay.Gate.Service.Account.Domain.ValueObjects;
using System;

namespace SideBay.Gate.Service.Account.Domain
{
    public class AccountAggregate : Entity<Guid>, IAggregateRoot
    {
        public AccountUserName UserName { get; private set; }
        public AccountPassword Password { get; private set; }
        public AccountFirstName FirstName { get; private set; }
        public AccountLastName LastName { get; private set; }
        public Email Email { get; private set; }
        public PhoneNumber PhoneNumber { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public bool Gender { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime LastActiveDate { get; private set; }
        public AccountAggregate(Guid id) : base(id) { }

        public AccountAggregate(Guid id,
            AccountFirstName firstName,
            AccountLastName lastName,
            Email email,
            AccountPassword password) : this(id)
        {
            this.SetUserName(email);
            this.SetEmail(email);
            this.SetFirstName(firstName);
            this.SetLastName(lastName);
            this.SetPassword(password);
            this.SetInstanceDateTime();
        }

        public void SetPassword(AccountPassword password)
        {
            this.Password = password;
        }

        public void SetFirstName(AccountFirstName firstName)
        {
            this.FirstName = firstName;
        }

        public void SetLastName(AccountLastName lastName)
        {
            this.LastName = lastName;
        }

        public void SetEmail(Email email)
        {
            this.Email = email;
        }

        public void SetPhoneNumber(PhoneNumber phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
        }

        public void SetDateOfBirth(DateTime dateOfBirth)
        {
            this.DateOfBirth = dateOfBirth;
        }

        public void SetGender(bool gender)
        {
            this.Gender = gender;
        }

        public void SetCity(string city)
        {
            this.City = city;
        }

        public void SetInstanceDateTime()
        {
            var current = DateTime.UtcNow;
            this.SetCreatedDate(current);
            this.SetLastActiveDate(current);
        }

        public void SetCreatedDate(DateTime createDate)
        {
            this.CreatedDate = createDate;
        }

        public void SetLastActiveDate(DateTime lastActiveDate)
        {
            this.LastActiveDate = lastActiveDate;
        }

        private void SetUserName(Email email)
        {
            this.UserName = email;
        }

        public void AddCreateAccountEvent()
        {
            var events = new AccountAddedEvent(Guid.NewGuid());
            events.SetData(this);
            AddDomainEvent(events);
        }
    }
}
