﻿using MediatR;
using SideBay.Gate.Core.Events;
using SideBay.Gate.Utilities.Enumes;
using System;

namespace SideBay.Gate.Service.Account.Domain.Events
{
    public class AccountAddedEvent : BaseEvent<Guid, AccountAggregate>, INotification
    {
        public AccountAddedEvent(Guid id) : base(id) { }
        public override string EventType => EventTypes.CREATE.ToString();
        public override string EventName => nameof(AccountAddedEvent).ToLower();
    }
}
