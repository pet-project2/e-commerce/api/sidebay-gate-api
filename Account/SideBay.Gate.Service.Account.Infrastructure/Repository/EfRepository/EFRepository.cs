﻿using Microsoft.EntityFrameworkCore;
using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository.EfRepository
{
    public class EFRepository<T> : IEfRepository<T> where T : class, IAggregateRoot
    {
        private readonly AppDbContext _context;
        private readonly DbSet<T> _dbSet;
        public EFRepository(AppDbContext context)
        {
            _context = context;
            this._dbSet = context.Set<T>();
        }

        public Task AddAsync(T entity)
        {
            _dbSet.AddAsync(entity);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(T entity)
        {
            if (entity == null || Equals(entity, default(T)))
            {
                throw new ArgumentNullException($"Entity type {typeof(T).Name} can not be null or default");
            }
            _dbSet.Remove(entity);
            return Task.CompletedTask;
        }

        public Task DeleteAsync<TId>(TId id)
        {
            if (id == null || Equals(id, default(TId)))
            {
                throw new ArgumentNullException($"Id type {typeof(T).Name} can not be null or default");
            }
            var entity = _dbSet.Find(id);
            return this.DeleteAsync(entity);
        }

        public Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return Task.FromResult(orderBy(query).AsEnumerable());
            }
            else
            {
                return Task.FromResult(query.AsEnumerable());
            }
        }

        public Task<T> GetByIdAsync<TId>(TId id)
        {
            if (id == null || Equals(id, default(TId)))
            {
                throw new ArgumentNullException($"Id type {typeof(T).Name} can not be null or default");
            }

            var entity = _dbSet.FindAsync(id).AsTask();
            _context.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public Task UpdateAsync(T entity)
        {
            if (entity == null || Equals(entity, default(T)))
            {
                throw new ArgumentNullException($"Entity type {typeof(T).Name} can not be null or default");
            }

            _context.Entry(entity).State = EntityState.Modified;
            return Task.CompletedTask;
        }
    }
}
