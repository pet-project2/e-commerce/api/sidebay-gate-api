﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Repository;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository
{
    public interface IAccountWriteRepository<T> : IWriteRepository<T> where T : class, IAggregateRoot
    {
        Task<bool> CreateUserAsync(AppAccount user, string password);
        Task<bool> AddClaims(AppAccount account);
    }
}
