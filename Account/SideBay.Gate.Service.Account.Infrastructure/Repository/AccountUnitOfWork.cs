﻿using MediatR;
using SideBay.Gate.Core.Models;
using SideBay.Gate.Service.Account.Infrastructure.Extensions;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository
{
    public class AccountUnitOfWork : IAccountUnitOfWork<AppAccount>
    {
        public IAccountReadRepository<AppAccount> AccoutnReadRepository { get; }
        public IAccountWriteRepository<AppAccount> AccountWriteRepository { get; }

        private readonly AppDbContext _dbContext;
        private readonly IMediator _mediator;
        public AccountUnitOfWork(IAccountReadRepository<AppAccount> accountReadRepository,
            IAccountWriteRepository<AppAccount> accountWriteRepository,
            AppDbContext dbContext,
            IMediator mediator)
        {
            this.AccoutnReadRepository = accountReadRepository;
            this.AccountWriteRepository = accountWriteRepository;
            this._dbContext = dbContext;
            this._mediator = mediator;
        }

        public async Task<bool> Commit()
        {
            var result = await _dbContext.SaveChangesAsync();
            return result > 0;
        }

        public Task Dispatch(IBaseModel model)
        {
            return _mediator.DispatchDomainEventsAsync(model);
        }
    }
}
