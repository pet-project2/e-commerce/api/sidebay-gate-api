﻿using SideBay.Gate.Core.Interface;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository.SearchRepository
{
    public interface ISearchRepository<T> where T : class, IAggregateRoot
    {

    }
}
