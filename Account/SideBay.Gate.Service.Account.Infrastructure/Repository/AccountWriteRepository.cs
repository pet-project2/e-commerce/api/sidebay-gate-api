﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using SideBay.Gate.Service.Account.Infrastructure.Helpers;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using SideBay.Gate.Service.Account.Infrastructure.Repository.EfRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository
{
    public class AccountWriteRepository : IAccountWriteRepository<AppAccount>
    {
        private readonly IEfRepository<AppAccount> _efRepository;
        private readonly UserManager<AppAccount> _userManager;
        private readonly ILogger<AccountWriteRepository> _logger;
        public AccountWriteRepository(IEfRepository<AppAccount> efRepository,
            UserManager<AppAccount> userManager,
            ILogger<AccountWriteRepository> logger)
        {
            this._efRepository = efRepository;
            this._userManager = userManager;
            this._logger = logger;
        }

        public Task AddAsync(AppAccount entity)
        {
            return this._efRepository.AddAsync(entity);
        }

        public Task<bool> AddClaims(AppAccount account)
        {
            var claims = AccountHelper.GetListClaim(account, "MERCHANT");
            return Task.FromResult(true);
        }

        public async Task<bool> CreateUserAsync(AppAccount user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            if (!result.Succeeded)
            {
                LogErr(result.Errors);
            }

            return result.Succeeded;
        }

        public Task DeleteAsync(AppAccount entity)
        {
            return this._efRepository.DeleteAsync(entity);
        }

        public Task DeleteAsync(string id)
        {
            return this._efRepository.DeleteAsync(id);
        }

        public Task UpdateAsync(AppAccount entity)
        {
            return this._efRepository.UpdateAsync(entity);
        }

        private void LogErr(IEnumerable<IdentityError> errors)
        {
            foreach (var error in errors)
            {
                _logger.LogError(error.Description);
            }
        }
    }
}
