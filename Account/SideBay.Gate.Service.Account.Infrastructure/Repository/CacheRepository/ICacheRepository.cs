﻿using SideBay.Gate.Core.Interface;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository.CacheRepository
{
    public interface ICacheRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetAsync(string key, CancellationToken cancellationToken = default);
        Task SetAsync(string key, T value);
    }
}
