﻿using Microsoft.Extensions.Caching.Distributed;
using SideBay.Gate.Core.Interface;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository.CacheRepository
{
    public class CacheRepository<T> : ICacheRepository<T> where T : class, IAggregateRoot
    {
        private readonly IDistributedCache _distributedCache;

        public CacheRepository(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public async Task<T> GetAsync(string key, CancellationToken cancellationToken = default)
        {
            var byteValue = await this._distributedCache.GetAsync(key);
            if (byteValue != null && byteValue.Length > 0)
            {
                var stringValue = Encoding.UTF8.GetString(byteValue);
                var value = JsonSerializer.Deserialize<T>(stringValue);
                return value;
            }
            return null;
        }

        public async Task SetAsync(string key, T value)
        {
            var jsonStringValue = JsonSerializer.Serialize(value);
            var valueEncode = Encoding.UTF8.GetBytes(jsonStringValue);
            await this._distributedCache.SetAsync(key, valueEncode);
        }
    }
}
