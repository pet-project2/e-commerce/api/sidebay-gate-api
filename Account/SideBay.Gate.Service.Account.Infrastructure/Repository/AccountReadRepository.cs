﻿using Microsoft.AspNetCore.Identity;
using SideBay.Gate.Core.Repository;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using SideBay.Gate.Service.Account.Infrastructure.Repository.CacheRepository;
using SideBay.Gate.Service.Account.Infrastructure.Repository.EfRepository;
using SideBay.Gate.Service.Account.Infrastructure.Repository.SearchRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository
{
    public class AccountReadRepository : IAccountReadRepository<AppAccount>
    {
        private ICacheRepository<AppAccount> _cacheRepository;
        private ISearchRepository<AppAccount> _searchRepository;
        private IEfRepository<AppAccount> _efRepository;
        private UserManager<AppAccount> _userManager;

        public AccountReadRepository(ICacheRepository<AppAccount> cacheRepository,
            ISearchRepository<AppAccount> searchRepository,
            IEfRepository<AppAccount> efRepository, UserManager<AppAccount> userManager)
        {
            this._cacheRepository = cacheRepository;
            this._searchRepository = searchRepository;
            this._efRepository = efRepository;
            this._userManager = userManager;
        }

        public async Task<bool> CheckAccountExistByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            email = email.Trim();
            var user = await this._userManager.FindByEmailAsync(email);
            return user != null;
        }

        public Task<IEnumerable<AppAccount>> GetAllAsync()
        {
            return this._efRepository.GetAsync();
        }

        public Task<AppAccount> GetByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Task.FromResult<AppAccount>(null);
            }

            return this._efRepository.GetByIdAsync(id);
        }
    }
}
