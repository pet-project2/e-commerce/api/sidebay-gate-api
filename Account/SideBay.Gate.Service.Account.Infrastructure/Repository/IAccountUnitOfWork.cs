﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Models;
using SideBay.Gate.Core.Repository;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository
{
    public interface IAccountUnitOfWork<T> : IUnitOfWork<T> where T : class, IAggregateRoot
    {
        public IAccountReadRepository<T> AccoutnReadRepository { get; }
        public IAccountWriteRepository<T> AccountWriteRepository { get; }
    }
}
