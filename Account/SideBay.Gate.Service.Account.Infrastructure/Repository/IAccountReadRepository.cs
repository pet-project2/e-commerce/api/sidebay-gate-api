﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Core.Repository;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Repository
{
    public interface IAccountReadRepository<T> : IReadRepository<T> where T : class, IAggregateRoot
    {
        Task<bool> CheckAccountExistByEmail(string email);
    }
}
