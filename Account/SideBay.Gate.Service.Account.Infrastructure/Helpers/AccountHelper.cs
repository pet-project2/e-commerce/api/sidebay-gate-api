﻿using IdentityModel;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace SideBay.Gate.Service.Account.Infrastructure.Helpers
{
    public class AccountHelper
    {
        public static IEnumerable<Claim> GetListClaim(AppAccount appAccount, string role)
        {
            yield return new Claim(JwtClaimTypes.Name, appAccount.UserName);
            yield return new Claim(JwtClaimTypes.Role, role);
        }
    }
}
