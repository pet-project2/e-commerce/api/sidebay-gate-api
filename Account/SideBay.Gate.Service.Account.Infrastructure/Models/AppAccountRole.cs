﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Models
{
    public class AppAccountRole : IdentityUserRole<Guid>
    {
        public AppAccount User { get; set; }
        public AppRole Role { get; set; }
    }
}
