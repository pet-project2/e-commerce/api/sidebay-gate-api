﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Account.Infrastructure.Models
{
    public class AppAccount : IdentityUser<Guid>, IAggregateRoot
    {
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastActiveDate { get; set; }
        public ICollection<AppAccountRole> UserRoles { get; set; }
        public IEnumerable<INotification> DomainEvents { get; set; }
    }
}
