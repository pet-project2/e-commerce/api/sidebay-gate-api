﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Account.Infrastructure.Models
{
    public class AppRole : IdentityRole<Guid>
    {
        public ICollection<AppAccountRole> UserRoles { get; set; }
    }
}
