﻿using AutoMapper;
using System;

namespace SideBay.Gate.Service.Account.Infrastructure.Mapper
{
    public static class InfrastructureMapper
    {
        public static IMapper Mapper => _lazy.Value;

        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AccountMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
