﻿using AutoMapper;
using SideBay.Gate.Service.Account.Domain;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SideBay.Gate.Service.Account.Utilities.Constants.Message.Account;

namespace SideBay.Gate.Service.Account.Infrastructure.Mapper
{
    public class AccountMapper : Profile
    {
        public AccountMapper()
        {
            CreateMap<AccountAggregate, AppAccount>()
                .ForMember(x => x.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(x => x.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(x => x.EmailConfirmed, opt => opt.MapFrom(src => false))
                .ForMember(x => x.NormalizedEmail, opt => opt.MapFrom(src => src.Email))
                .ForMember(x => x.NormalizedUserName, opt => opt.MapFrom(src => src.Email))
                .ForMember(x => x.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber))
                .ForMember(x => x.PhoneNumberConfirmed, opt => opt.MapFrom(src => false))
                .ForMember(x => x.City, opt => opt.MapFrom(src => src.City))
                .ForMember(x => x.ConcurrencyStamp, opt => opt.MapFrom(src => Guid.NewGuid().ToString()))
                .ForMember(x => x.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(x => x.DateOfBirth, opt => opt.MapFrom(src => src.DateOfBirth))
                .ForMember(x => x.Gender, opt => opt.MapFrom(src => src.Gender ? Gender.MALE : Gender.FEMALE))
                .ForMember(x => x.DomainEvents, opt => opt.MapFrom((src, des, result) =>
                {
                    result = src.DomainEvents.ToList();
                    src.ClearDomainEvents();
                    return result;
                }))
                .ForMember(x => x.LastActiveDate, opt => opt.MapFrom(src => src.LastActiveDate));
        }
    }
}
