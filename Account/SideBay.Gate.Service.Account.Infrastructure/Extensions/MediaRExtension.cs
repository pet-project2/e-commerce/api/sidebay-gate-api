﻿using MediatR;
using SideBay.Gate.Core.Models;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Extensions
{
    public static class MediaRExtension
    {
        public static Task DispatchDomainEventsAsync(this IMediator mediator, IBaseModel model)
        {
            foreach (var domainEvent in model.DomainEvents)
            {
                mediator.Publish(domainEvent);
            }

            return Task.CompletedTask;
        }
    }
}
