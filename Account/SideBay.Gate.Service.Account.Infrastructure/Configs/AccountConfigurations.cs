﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SideBay.Gate.Service.Account.Infrastructure.Models;

namespace SideBay.Gate.Service.Account.Infrastructure.Configs
{
    public class AccountConfigurations : IEntityTypeConfiguration<AppAccount>
    {
        public void Configure(EntityTypeBuilder<AppAccount> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Ignore(x => x.DomainEvents);

            builder.HasMany(ur => ur.UserRoles)
                .WithOne(u => u.User)
                .HasForeignKey(u => u.UserId)
                .IsRequired();
        }
    }
}
