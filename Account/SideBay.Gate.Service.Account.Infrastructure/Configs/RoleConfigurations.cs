﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SideBay.Gate.Service.Account.Infrastructure.Models;

namespace SideBay.Gate.Service.Account.Infrastructure.Configs
{
    public class RoleConfigurations : IEntityTypeConfiguration<Models.AppRole>
    {
        public void Configure(EntityTypeBuilder<AppRole> builder)
        {
            builder.HasKey(x => x.Id);

            builder
                .HasMany(ur => ur.UserRoles)
                .WithOne(u => u.Role)
                .HasForeignKey(u => u.UserId)
                .IsRequired();
        }
    }
}
