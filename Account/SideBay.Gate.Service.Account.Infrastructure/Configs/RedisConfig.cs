﻿namespace SideBay.Gate.Service.Account.Infrastructure.Configs
{
    public class RedisConfig
    {
        public string Uri { get; set; }
        public string Port { get; set; }
        public string AbsoluteExpiredTime { get; set; }
        public string SlidingExpiredTime { get; set; }
    }
}
