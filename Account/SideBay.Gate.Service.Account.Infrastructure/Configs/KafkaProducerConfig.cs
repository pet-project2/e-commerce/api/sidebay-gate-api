﻿namespace SideBay.Gate.Service.Account.Infrastructure.Configs
{
    public class KafkaProducerConfig
    {
        public string Uri { get; set; }
        public string Port { get; set; }
        public string Topic { get; set; }
    }
}
