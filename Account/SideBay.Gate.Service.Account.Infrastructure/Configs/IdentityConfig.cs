﻿namespace SideBay.Gate.Service.Account.Infrastructure.Configs
{
    public class IdentityConfig
    {
        public string Uri { get; set; }
        public string Audience { get; set; }
        public string Key { get; set; }
    }
}
