﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SideBay.Gate.Service.Account.Infrastructure.Models;

namespace SideBay.Gate.Service.Account.Infrastructure.Configs
{
    public class AccountRoleConfigurations : IEntityTypeConfiguration<Models.AppAccountRole>
    {
        public void Configure(EntityTypeBuilder<AppAccountRole> builder)
        {
            builder.HasKey(x => new { x.UserId, x.RoleId });

            builder
                .HasOne(u => u.User)
                .WithMany(x => x.UserRoles)
                .HasForeignKey(x => x.UserId)
                .IsRequired();

            builder
                .HasOne(u => u.Role)
                .WithMany(x => x.UserRoles)
                .HasForeignKey(x => x.RoleId)
                .IsRequired();
        }
    }
}
