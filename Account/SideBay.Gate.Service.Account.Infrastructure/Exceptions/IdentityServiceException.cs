﻿using System;

namespace SideBay.Gate.Service.Account.Infrastructure.Exceptions
{
    public class IdentityServiceException : Exception
    {
        public IdentityServiceException(string message) : base(message)
        {

        }
    }
}
