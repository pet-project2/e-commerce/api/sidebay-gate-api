﻿using SideBay.Gate.Core.Interface;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Service
{
    public interface IIdentityService<T> where T : class, IAggregateRoot
    {
        Task<string> GetTokenAccessAsync(string username, string password);
        Task<string> GenerateAccessToken(AppAccount account);
    }
}
