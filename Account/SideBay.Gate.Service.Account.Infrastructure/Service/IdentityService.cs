﻿using IdentityModel;
using IdentityModel.Client;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SideBay.Gate.Service.Account.Infrastructure.Configs;
using SideBay.Gate.Service.Account.Infrastructure.Exceptions;
using SideBay.Gate.Service.Account.Infrastructure.Helpers;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Infrastructure.Service
{
    public class IdentityService : IIdentityService<AppAccount>
    {
        private readonly HttpClient _httpClient;
        private readonly IdentityConfig _appConfig;
        public IdentityService(HttpClient httpClient,
            IOptions<IdentityConfig> options)
        {
            this._appConfig = options.Value;
            this._httpClient = httpClient;
            httpClient.BaseAddress = new Uri(_appConfig.Uri);
        }

        public async Task<string> GenerateAccessToken(AppAccount account)
        {
            var now = DateTime.UtcNow;
            var issuedAt = now;
            var expires = now.AddHours(1);
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this._appConfig.Key));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var claims = AccountHelper.GetListClaim(account, "MERCHANT");
            var claimIdentity = new ClaimsIdentity(claims);

            var securityToken = new JwtSecurityTokenHandler().CreateJwtSecurityToken(
                        issuer: this._appConfig.Uri,
                        audience: this._appConfig.Audience,
                        subject: claimIdentity,
                        notBefore: issuedAt,
                        expires: expires,
                        signingCredentials: signingCredentials);

            var token = new JwtSecurityTokenHandler().WriteToken(securityToken);

            return await Task.FromResult(token);


        }

        public async Task<string> GetTokenAccessAsync(string username, string password)
        {
            var disco = await _httpClient.GetDiscoveryDocumentAsync();
            if (disco.IsError)
                throw new IdentityServiceException(disco.Error);

            var tokenResponse = await this._httpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                UserName = username,
                Password = password
            });

            if (tokenResponse.IsError)
                throw new IdentityServiceException(tokenResponse.Error);

            return tokenResponse.AccessToken;
        }
    }
}
