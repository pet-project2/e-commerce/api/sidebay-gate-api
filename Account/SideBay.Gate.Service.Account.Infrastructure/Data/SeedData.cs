﻿using IdentityModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using SideBay.Gate.Service.Account.Infrastructure.Helpers;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static SideBay.Gate.Service.Account.Utilities.Constants.Message;

namespace SideBay.Gate.Service.Account.Infrastructure.Data
{
    public class SeedData
    {
        public static async Task InitializeUserData(AppDbContext context, IServiceProvider servicesProvider)
        {
            using (var scope = servicesProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                context.Database.Migrate();

                var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<AppAccount>>();
                var alice = await userMgr.FindByEmailAsync("alice@gmail.com");
                if (alice == null)
                {
                    alice = new AppAccount
                    {
                        UserName = "alice@gmail.com",
                        Email = "alice@gmail.com",
                        EmailConfirmed = true,
                    };

                    var result = await userMgr.CreateAsync(alice, "abcd1234");
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var claims = AccountHelper.GetListClaim(alice, "ADMIN");
                    result = await userMgr.AddClaimsAsync(alice, new Claim[]{
                                new Claim(JwtClaimTypes.Name, "Alice Smith")
                            });
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                    Log.Debug("alice created");
                }
                else
                {
                    Log.Debug("alice already exists");
                }

                var bob = await userMgr.FindByEmailAsync("bob@gmail.com");
                if (bob == null)
                {
                    bob = new AppAccount
                    {
                        UserName = "bob@gmail.com",
                        Email = "bob@gmail.com",
                        EmailConfirmed = true
                    };
                    var result = await userMgr.CreateAsync(bob, "abcd1234");
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var claims = AccountHelper.GetListClaim(bob, "MERCHANT");
                    result = await userMgr.AddClaimsAsync(bob, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "Bob Smith"),
                        });
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                    Log.Debug("bob created");
                }
                else
                {
                    Log.Debug("bob already exists");
                }
            }
        }
    }
}
