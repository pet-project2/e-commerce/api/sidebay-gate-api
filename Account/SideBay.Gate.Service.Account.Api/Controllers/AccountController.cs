﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SideBay.Gate.Core.Dtos.Apis;
using SideBay.Gate.Service.Account.Application.Command.Create;
using SideBay.Gate.Utilities.Enumes;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<AccountController> _logger;
        public AccountController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        [Route("register")]
        [ProducesResponseType(typeof(ApiCommandResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RegisterAccount([FromBody]CreateAccountCommand command)
        {
            var result = await _mediator.Send(command);
            if (!result.IsSuccess)
            {
                return StatusCode(
                   StatusCodes.Status500InternalServerError,
                   new ApiErrorResponse<string>
                   (
                       "Somthing happend when create new Account",
                       StatusCodes.Status500InternalServerError,
                       ErrorType.INTERNAL_SERVER_ERROR,
                       result.Message
                   ));
            }

            return StatusCode(
                    StatusCodes.Status201Created,
                    new ApiCommandResponse(result.Message, StatusCodes.Status201Created));
        }
    }
}
