using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SideBay.Gate.Service.Account.Api.Extensions;
using SideBay.Gate.Service.Account.Infrastructure;
using SideBay.Gate.Service.Account.Infrastructure.Data;

namespace SideBay.Gate.Service.Account.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build()
                .MigrateDbContext<AppDbContext>( async (context, service) =>
                {
                   await SeedData.InitializeUserData(context, service);
                })
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
