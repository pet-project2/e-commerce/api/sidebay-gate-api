﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using SideBay.Gate.Service.Account.Infrastructure.Configs;
using System.Text;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class IdentityServerExtensions
    {
        public static void AddIds4(this IServiceCollection service, IConfiguration configuration)
        {
            var identityServerConfig = configuration.GetSection("IdentityServer")
                                                    .Get<IdentityConfig>();

            service.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.Authority = identityServerConfig.Uri;
                    options.Audience = identityServerConfig.Audience;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidateLifetime= true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(identityServerConfig.Key))
                    };
                });

        }
    }
}
