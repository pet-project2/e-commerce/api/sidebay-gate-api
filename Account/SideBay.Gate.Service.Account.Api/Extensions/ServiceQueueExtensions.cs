﻿using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Account.Infrastructure.Configs;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class ServiceQueueExtensions
    {
        public static void AddQueue<TKey, TValue>(this IServiceCollection services, IConfiguration configuration)
        {
            var kafkaConfig = configuration.GetSection("Kafka");
            var kafkaSettings = kafkaConfig.Get<KafkaProducerConfig>();

            // binding setting
            services.Configure<KafkaProducerConfig>(kafkaConfig);

            var producerConfig = new ProducerConfig
            {
                BootstrapServers = kafkaSettings.Uri,
                Debug = "topic,msg"
            };

            var producerBuilder = new ProducerBuilder<TKey, TValue>(producerConfig).Build();
            services.AddSingleton(producerBuilder);
        }
    }
}
