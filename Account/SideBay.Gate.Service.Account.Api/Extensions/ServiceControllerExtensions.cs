﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Account.Api.Filter;
using SideBay.Gate.Service.Account.Application.Command.Create;
using SideBay.Gate.Service.Account.Application.Mapper;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class ServiceControllerExtensions
    {
        public static void RegistryController(this IServiceCollection services)
        {
            // validation
            services.AddControllers().ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            })
            .AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                opts.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(new ValidationFilter());
            });

            services.AddFluentValidationAutoValidation();

            var assembly = typeof(ApplicationMapper).GetTypeInfo().Assembly;
            services.AddValidatorsFromAssembly(assembly);
        }
    }
}
