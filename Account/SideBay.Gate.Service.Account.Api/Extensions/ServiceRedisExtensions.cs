﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Account.Infrastructure.Configs;
using SideBay.Gate.Service.Account.Infrastructure.Repository.CacheRepository;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class ServiceRedisExtensions
    {
        public static void AddRedis(this IServiceCollection services, IConfiguration configuration)
        {
            var redisConfig = configuration.GetSection("Redis");
            var redisSettings = redisConfig.Get<RedisConfig>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = $"{redisSettings.Uri}:{redisSettings.Port}";
            });

            services.Configure<RedisConfig>(redisConfig);
            services.AddScoped(typeof(ICacheRepository<>), typeof(CacheRepository<>));
        }
    }
}
