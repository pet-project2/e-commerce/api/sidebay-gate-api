﻿using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using SideBay.Gate.Service.Account.Infrastructure.Repository.SearchRepository;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class ServiceSearchExtensions
    {
        public static void AddSearchService(this IServiceCollection services)
        {
            services.AddScoped<ISearchRepository<AppAccount>, SearchRepository>();
        }
    }
}
