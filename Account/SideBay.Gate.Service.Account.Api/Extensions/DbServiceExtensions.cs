﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Account.Infrastructure;
using SideBay.Gate.Service.Account.Infrastructure.Repository.EfRepository;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class DbServiceExtensions
    {
        public static void AddDatabase(this IServiceCollection service, IConfiguration configuration)
        {
            var dbConnection = configuration.GetConnectionString("SideGateBay");

            service.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(dbConnection);
            });

            service.AddScoped(typeof(IEfRepository<>), typeof(EFRepository<>));
        }
    }
}
