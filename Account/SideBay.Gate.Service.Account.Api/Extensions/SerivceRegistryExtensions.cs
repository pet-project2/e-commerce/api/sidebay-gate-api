﻿using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using SideBay.Gate.Service.Account.Infrastructure.Repository;

namespace SideBay.Gate.Service.Account.Api.Extensions
{
    public static class SerivceRegistryExtensions
    {
        public static void RegistryService(this IServiceCollection services)
        {
            services.AddScoped<IAccountReadRepository<AppAccount>, AccountReadRepository>();
            services.AddScoped<IAccountWriteRepository<AppAccount>, AccountWriteRepository>();
            services.AddScoped<IAccountUnitOfWork<AppAccount>, AccountUnitOfWork>();
        }
    }
}
