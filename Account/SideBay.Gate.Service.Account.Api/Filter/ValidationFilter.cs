﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SideBay.Gate.Core.Dtos.Apis;
using SideBay.Gate.Utilities.Enumes;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SideBay.Gate.Service.Account.Api.Filter
{
    /// <summary>
    /// https://medium.com/@sergiobarriel/how-to-automatically-validate-a-model-with-mvc-filter-and-fluent-validation-package-ae51098bcf5b
    /// </summary>
    public class ValidationFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                IEnumerable<string> allErrors = context.ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(x => x.ErrorMessage);

                var apiErrResponse = new ApiErrorResponse<IEnumerable<string>>("Model is not valid",
                    StatusCodes.Status400BadRequest,
                    ErrorType.MODEL_INVALID,
                    allErrors);
                context.Result = new BadRequestObjectResult(apiErrResponse);
            }
        }
    }
}
