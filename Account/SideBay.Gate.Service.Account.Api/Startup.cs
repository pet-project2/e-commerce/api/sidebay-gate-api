using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SideBay.Gate.Service.Account.Api.Extensions;
using SideBay.Gate.Service.Account.Application.Mapper;
using SideBay.Gate.Service.Account.Infrastructure.Mapper;
using System.Reflection;

namespace SideBay.Gate.Service.Account.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.RegistryController();

            services.AddDatabase(Configuration);
            services.AddIdentityServices();
            services.AddIds4(Configuration);

            var assemblies = new[]
            {
                typeof(ApplicationMapper).GetTypeInfo().Assembly,
                typeof(InfrastructureMapper).GetTypeInfo().Assembly
            };

            services.AddMediatR(assemblies);
            services.AddAutoMapper(assemblies);

            services.RegistryService();

            // Third party Service
            services.AddQueue<string, string>(Configuration);
            services.AddRedis(Configuration);
            services.AddSearchService();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SideBay.Gate.Service.Account.Api", Version = "v1" });
            });

            // health check api
            services.AddHealthChecks();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SideBay.Gate.Service.Account.Api v1"));
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
