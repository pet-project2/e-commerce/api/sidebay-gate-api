﻿using AutoMapper;
using SideBay.Gate.Service.Account.Application.Command.Create;
using SideBay.Gate.Service.Account.Domain;
using SideBay.Gate.Service.Account.Domain.ValueObjects;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using System;

namespace SideBay.Gate.Service.Account.Application.Mapper
{
    public class CreateAccountCommandMapper : Profile
    {
        public CreateAccountCommandMapper()
        {
            CreateMap<CreateAccountCommand, AccountAggregate>()
                .ConstructUsing(x => new AccountAggregate(
                    Guid.NewGuid(),
                    AccountFirstName.FromString(x.FirstName),
                    AccountLastName.FromString(x.LastName),
                    Email.FromString(x.Email),
                    AccountPassword.FromString(x.Password)));
        }
    }
}
