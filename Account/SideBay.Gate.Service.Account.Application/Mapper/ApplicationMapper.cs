﻿using AutoMapper;
using System;

namespace SideBay.Gate.Service.Account.Application.Mapper
{
    public static class ApplicationMapper
    {
        public static IMapper Mapper => _lazy.Value;

        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CreateAccountCommandMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
