﻿using MediatR;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using SideBay.Gate.Service.Account.Infrastructure.Service;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Application.Query.GetAccessToken
{
    public class GetAccessTokenQueryHandler : IRequestHandler<GetAccessTokenQuery, string>
    {
        private readonly IIdentityService<AppAccount> _identityService;
        public async Task<string> Handle(GetAccessTokenQuery request, CancellationToken cancellationToken)
        {
            var userName = request.UserName;
            var passWord = request.Password;

            return await this._identityService.GetTokenAccessAsync(userName, passWord);
        }
    }
}
