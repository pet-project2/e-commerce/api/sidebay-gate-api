﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace SideBay.Gate.Service.Account.Application.Query.GetAccessToken
{
    public class GetAccessTokenQuery : IRequest<string>
    {
        [Required(ErrorMessage = "Username must not be null or empty")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password must not be null or empty")]
        public string Password { get; set; }
    }
}
