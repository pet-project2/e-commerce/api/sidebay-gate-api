﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Account.Application.Command.Create
{
    public class CreateAccountCommandValidation : AbstractValidator<CreateAccountCommand>
    {
        public CreateAccountCommandValidation()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("FirstName must not be empty")
                .NotNull().WithMessage("FirstName must not be null");

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("LastName must not be empty")
                .NotNull().WithMessage("LastName must not be null");

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email must not be empty")
                .NotNull().WithMessage("Email must not be null")
                .EmailAddress().WithMessage("Invalid email address");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Password must not be empty")
                .NotNull().WithMessage("Password must not be null");

            RuleFor(x => x.PasswordConfirm)
                .NotEmpty().WithMessage("Confirm Password must not be empty")
                .NotNull().WithMessage("Confirm Password must not be null")
                .Equal(x => x.Password)
                .When(x => !string.IsNullOrEmpty(x.Password) && !string.IsNullOrEmpty(x.PasswordConfirm))
                .WithMessage("Confirm Password must match Password");
        }
    }
}
