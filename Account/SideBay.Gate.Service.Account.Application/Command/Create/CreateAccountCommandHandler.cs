﻿using MediatR;
using SideBay.Gate.Core.Dtos.Applications;
using SideBay.Gate.Service.Account.Infrastructure.Models;
using SideBay.Gate.Service.Account.Infrastructure.Repository;
using SideBay.Gate.Utilities.Enumes;
using System.Threading;
using System.Threading.Tasks;
using SideBay.Gate.Service.Account.Utilities.Constants;
using SideBay.Gate.Service.Account.Application.Mapper;
using SideBay.Gate.Service.Account.Domain;
using SideBay.Gate.Service.Account.Infrastructure.Mapper;

namespace SideBay.Gate.Service.Account.Application.Command.Create
{
    public class CreateAccountCommandHandler : IRequestHandler<CreateAccountCommand, IResult<Unit>>
    {
        private readonly IAccountUnitOfWork<AppAccount> _unitOfWork;

        public CreateAccountCommandHandler(IAccountUnitOfWork<AppAccount> unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IResult<Unit>> Handle(CreateAccountCommand request, CancellationToken cancellationToken)
        {
            var email = request.Email;
            var isExist = await _unitOfWork.AccoutnReadRepository.CheckAccountExistByEmail(email);
            if(isExist)
            {
                var failure = new Failure(ErrorType.EXISTED,Message.Account.ACCOUNT_ERROR_EXISTED);
                return Result<Unit>.Failed("Can not create account cause account is existed", failure);
            }

            var accountAggregate = ApplicationMapper.Mapper.Map<AccountAggregate>(request);
            accountAggregate.AddCreateAccountEvent();

            var account = InfrastructureMapper.Mapper.Map<AppAccount>(accountAggregate);

            var isSuccess = await this._unitOfWork.AccountWriteRepository.CreateUserAsync(account, request.Password);
            if (!isSuccess)
            {
                var failure = new Failure(ErrorType.CREATED, Message.Account.ACCOUNT_ERROR_CREATED);
                return Result<Unit>.Failed("Can not create new account cause can not saved new account", failure);
            }

            return Result<Unit>.Success(Message.Account.ACCOUNT_CREATED_SUUCESS);
        }
    }
}
