﻿using MediatR;
using SideBay.Gate.Core.Dtos.Applications;
using System;
using System.ComponentModel.DataAnnotations;

namespace SideBay.Gate.Service.Account.Application.Command.Create
{
    public class CreateAccountCommand : IRequest<IResult<Unit>>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}
