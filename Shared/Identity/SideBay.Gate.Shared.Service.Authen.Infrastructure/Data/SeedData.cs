﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.EntityFrameworkCore;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations;
using System.Linq;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Data
{
    public class SeedData
    {
        public static void InitializePersistedGrantDatabase(PersistedGrantDbContext context)
        {
            context.Database.Migrate();
        }

        public static void InitializeConfigurationDatabase(ConfigurationDbContext context)
        {
            context.Database.Migrate();
            if (!context.Clients.Any())
            {
                foreach (var client in IdentityConfigs.Clients)
                {
                    context.Clients.Add(client.ToEntity());
                }
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in IdentityConfigs.IdentityResources)
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
            }

            if (!context.ApiScopes.Any())
            {
                foreach (var resource in IdentityConfigs.ApiScopes)
                {
                    context.ApiScopes.Add(resource.ToEntity());
                }
            }

            if (!context.ApiResources.Any())
            {
                foreach (var resource in IdentityConfigs.ApiResources)
                {
                    context.ApiResources.Add(resource.ToEntity());
                }
            }

            context.SaveChanges();
        }
    }
}
