﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;
using System;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure
{
    public class AppDbContext : IdentityDbContext<AppAccount, AppRole, Guid,
        IdentityUserClaim<Guid>, AppAccountRole, IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AccountConfigurations());
            modelBuilder.ApplyConfiguration(new RoleConfigurations());
            modelBuilder.ApplyConfiguration(new AccountRoleConfigurations());
        }
    }
}
