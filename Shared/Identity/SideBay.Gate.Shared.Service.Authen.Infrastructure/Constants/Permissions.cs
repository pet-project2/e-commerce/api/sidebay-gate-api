﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Constants
{
    public struct Permissions
    {
        public const string PERMISSION = "Permission";

        public List<string> GeneratePermissionsByModule(string module) => new List<string>()
        {
            $"Permissions.{module}.Create",
            $"Permissions.{module}.View",
            $"Permissions.{module}.Edit",
            $"Permissions.{module}.Delete",
        };
    }

    public enum PermissionType
    {
        [Description("Permission Type")]
        PERMISSION = 0
    }

    public enum ModuleType
    {
        Category = 0,
        Product = 1
    }
}
