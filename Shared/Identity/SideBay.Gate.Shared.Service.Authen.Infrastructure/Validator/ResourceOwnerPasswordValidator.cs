﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;
using System;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Validator
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly UserManager<AppAccount> _userManager;
        private readonly SignInManager<AppAccount> _signInManager;
        public ResourceOwnerPasswordValidator(UserManager<AppAccount> userManager, SignInManager<AppAccount> signInManager)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
        }
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var result = await _signInManager.PasswordSignInAsync(context.UserName, context.Password,
                                                                   isPersistent: false, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                var user = await _userManager.FindByEmailAsync(context.UserName);
                context.Result = new GrantValidationResult(user.Id.ToString(),
                    authenticationMethod: "custom");
            }
            else
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant,
                    "invalid custom credential");
            }
        }
    }
}
