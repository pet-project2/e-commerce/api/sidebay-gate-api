﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations
{
    public class RoleConfigurations : IEntityTypeConfiguration<AppRole>
    {
        public void Configure(EntityTypeBuilder<AppRole> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Ignore(x => x.DomainEvents);

            builder
                .HasMany(ur => ur.AccountRoles)
                .WithOne(u => u.Role)
                .HasForeignKey(u => u.RoleId);
        }
    }
}
