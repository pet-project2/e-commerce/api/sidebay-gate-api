﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;
using System;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations
{
    public class AccountRoleConfigurations : IEntityTypeConfiguration<AppAccountRole>
    {
        public void Configure(EntityTypeBuilder<AppAccountRole> builder)
        {
            builder.HasKey(x => new { x.UserId, x.RoleId });

            builder
                .HasOne(u => u.Account)
                .WithMany(x => x.AccountRoles)
                .HasForeignKey(x => x.UserId)
                .IsRequired();

            builder
                .HasOne(u => u.Role)
                .WithMany(x => x.AccountRoles)
                .HasForeignKey(x => x.RoleId)
                .IsRequired();
        }
    }
}
