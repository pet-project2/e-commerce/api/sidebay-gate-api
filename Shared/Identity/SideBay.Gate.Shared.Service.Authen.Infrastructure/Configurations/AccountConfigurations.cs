﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations
{
    public class AccountConfigurations : IEntityTypeConfiguration<AppAccount>
    {
        public void Configure(EntityTypeBuilder<AppAccount> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Ignore(x => x.DomainEvents);

            builder
                .HasMany(ur => ur.AccountRoles)
                .WithOne(u => u.Account)
                .HasForeignKey(u => u.UserId)
                .IsRequired();
        }
    }
}
