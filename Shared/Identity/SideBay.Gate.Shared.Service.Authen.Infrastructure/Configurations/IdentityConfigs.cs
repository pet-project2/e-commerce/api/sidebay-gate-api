﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations
{
    public static class IdentityConfigs
    {
        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "SideBayApi",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "product",
                        "account"
                    }
                },
                new Client
                {
                    ClientId = "SideBayApiWebApp",
                    ClientName = "Side Bay Gate Web App",
                    AllowAccessTokensViaBrowser = true,
                    RedirectUris =
                    {
                      "http://localhost:4200",
                      "https://localhost:4200"
                    },
                    AllowedGrantTypes = GrantTypes.CodeAndClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "product"
                    },
                    AllowedCorsOrigins = 
                    { 
                        "http://localhost:4200",
                        "https://localhost:4200"
                    },
                    PostLogoutRedirectUris =
                    {
                        "http://localhost:4200",
                        "https://localhost:4200"
                    },
                    RequireClientSecret = true,
                    RequireConsent = true,
                    RequirePkce = true,
                    AccessTokenLifetime = 1800
                },
            };

        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource()
                {
                    Name = "product.res",
                    DisplayName = "product api",
                    Scopes = { "product" },
                    ShowInDiscoveryDocument = true,
                    ApiSecrets =
                    {
                        new Secret("secretp".Sha256())
                    },
                    UserClaims =
                    {
                        JwtClaimTypes.Subject,
                        JwtClaimTypes.Address,
                        JwtClaimTypes.BirthDate,
                        JwtClaimTypes.Expiration,
                        JwtClaimTypes.Role,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.Name
                    },
                    Description = "description product api",
                    Enabled = true
                    
                },
                new ApiResource()
                {
                    Name = "account.res",
                    DisplayName = "account api",
                    Scopes = { "account" },
                    ShowInDiscoveryDocument = true,
                    ApiSecrets =
                    {
                        new Secret("account".Sha256())
                    },
                    UserClaims =
                    {
                        JwtClaimTypes.Subject,
                        JwtClaimTypes.Address,
                        JwtClaimTypes.BirthDate,
                        JwtClaimTypes.Expiration,
                        JwtClaimTypes.Role,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.Name
                    },
                    Description = "description account api",
                    Enabled = true
                }
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope(name: "product"),
                new ApiScope(name: "account")
            };
    }
}
