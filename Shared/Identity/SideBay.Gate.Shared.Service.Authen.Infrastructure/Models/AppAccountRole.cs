﻿using Microsoft.AspNetCore.Identity;
using System;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Models
{
    public class AppAccountRole : IdentityUserRole<Guid>
    {
        public virtual AppAccount Account { get; set; }
        public virtual AppRole Role { get; set; }
    }
}
