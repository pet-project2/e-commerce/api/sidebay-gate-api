﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;

namespace SideBay.Gate.Shared.Service.Authen.Infrastructure.Models
{
    public class AppRole : IdentityRole<Guid>, IAggregateRoot
    {
        public DateTime CreatedDate { get; set; }
        public virtual ICollection<AppAccountRole> AccountRoles { get; set; }
        public IEnumerable<INotification> DomainEvents { get; set; }
    }
}
