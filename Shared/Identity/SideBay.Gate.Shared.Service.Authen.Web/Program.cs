using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace SideBay.Gate.Shared.Service.Authen.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build()
                //.MigrateDbContext<PersistedGrantDbContext>((context, services) =>
                //{
                //    SeedData.InitializePersistedGrantDatabase(context);
                //})
                //.MigrateDbContext<ConfigurationDbContext>((context, services) =>
                //{
                //    SeedData.InitializeConfigurationDatabase(context);
                //})
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
