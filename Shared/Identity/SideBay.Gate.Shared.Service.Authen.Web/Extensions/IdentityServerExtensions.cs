﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Configurations;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Models;
using SideBay.Gate.Shared.Service.Authen.Infrastructure.Validator;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace SideBay.Gate.Shared.Service.Authen.Web.Extensions
{
    public static class IdentityServerExtensions
    {
        public static void AddIds4Server(this IServiceCollection service, IConfiguration configuration)
        {
            var assembly = typeof(IdentityConfigs).GetTypeInfo().Assembly.GetName().Name;
            var connectionString = configuration.GetConnectionString("SideGateIdentity");

            service.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;
                options.EmitStaticAudienceClaim = false;
            })
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(assembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b => b.UseSqlServer(connectionString,
                        sql => sql.MigrationsAssembly(assembly)); 
                })
                .AddAspNetIdentity<AppAccount>()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddSigningCredential(new X509Certificate2("key\\sidebaygate.pfx"));
        }
    }
}
