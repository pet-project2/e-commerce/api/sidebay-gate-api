using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using SideBay.Gate.Shared.Service.File.Api.Exceptions;
using SideBay.Gate.Shared.Service.File.Api.Extensions;
using SideBay.Gate.Shared.Service.File.Application.Command.Create;
using SideBay.Gate.Shared.Service.File.Infrastructure;
using System;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SideBay.Gate.Shared.Service.File.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.AddControllers()
                .AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                opts.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SideBay.Gate.Shared.Service.File.Api", Version = "v1" });
            });

            // Database
            var dbConnection = this.Configuration.GetConnectionString("SideGateBay");
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(dbConnection);
            });


            var assemblies = new[]
            {
                typeof(CreateFileUploadCommand).GetTypeInfo().Assembly,
                typeof(AppDbContext).GetTypeInfo().Assembly
            };

            services.AddMediatR(assemblies);
            services.AddAutoMapper(assemblies);

            services.RegistryService();
            services.RegistryConfig(Configuration);

            services.AddHealthChecks();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SideBay.Gate.Shared.Service.File.Api v1"));
            }

            app.ConfigureExceptionHandler(logger);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
