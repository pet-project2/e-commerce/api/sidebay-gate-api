﻿using System;

namespace SideBay.Gate.Shared.Service.File.Api.Exceptions
{
    public class FileApiException : Exception
    {
        public FileApiException(string message) : base(message)
        {

        }
    }
}
