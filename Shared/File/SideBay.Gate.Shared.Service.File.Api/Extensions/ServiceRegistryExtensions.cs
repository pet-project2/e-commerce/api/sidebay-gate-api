﻿using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Shared.Service.File.Infrastructure.Repository;

namespace SideBay.Gate.Shared.Service.File.Api.Extensions
{
    public static class ServiceRegistryExtensions
    {
        public static void RegistryService(this IServiceCollection services)
        {
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
