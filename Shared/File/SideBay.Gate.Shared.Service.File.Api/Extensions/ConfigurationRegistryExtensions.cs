﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Shared.Service.File.Infrastructure.Configs;

namespace SideBay.Gate.Shared.Service.File.Api.Extensions
{
    public static class ConfigurationRegistryExtensions
    {
        public static void RegistryConfig(this IServiceCollection services, IConfiguration configuration)
        {
            var fileStorePath = configuration.GetSection("File");
            services.Configure<AppConfiguration>(fileStorePath);
        }
    }
}
