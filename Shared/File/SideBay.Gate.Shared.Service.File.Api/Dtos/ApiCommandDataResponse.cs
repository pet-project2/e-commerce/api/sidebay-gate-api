﻿using SideBay.Gate.Core.Dtos.Apis;

namespace SideBay.Gate.Shared.Service.File.Api.Dtos
{
    public class ApiCommandDataResponse<T> : ApiCommandResponse
    {
        public T Data { get; private set; }
        public ApiCommandDataResponse(string message, int statusCode) : base(message, statusCode)
        {
        }

        public ApiCommandDataResponse(string message, int statusCode, T data) : this(message, statusCode)
        {
            this.Data = data;
        }
    }
}
