﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SideBay.Gate.Core.Dtos.Apis;
using SideBay.Gate.Shared.Service.File.Api.Dtos;
using SideBay.Gate.Shared.Service.File.Application.Command.Create;
using SideBay.Gate.Shared.Service.File.Application.Dtos;
using SideBay.Gate.Shared.Service.File.Infrastructure.Configs;
using SideBay.Gate.Utilities.Enumes;
using System.IO;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly AppConfiguration _appConfiguration;

        public FileController(IMediator mediator, IOptions<AppConfiguration> options)
        {
            _mediator = mediator;
            this._appConfiguration = new AppConfiguration
            {
                DirectoryPath = options.Value.DirectoryPath
            };
        }

        [HttpPost]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ApiCommandResponse), StatusCodes.Status201Created)]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            var fileUploadCommand = new CreateFileUploadCommand();
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileUploadCommand.SetContent(ms.ToArray());
                fileUploadCommand.SetFileType(file.ContentType);
                fileUploadCommand.SetFileName(file.FileName);
            }

            fileUploadCommand.SetPath(this._appConfiguration.DirectoryPath);

            var result = await this._mediator.Send(fileUploadCommand);

            if (!result.IsSuccess)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new ApiErrorResponse<string>
                    (
                        "Somthing happend when create new product",
                        StatusCodes.Status500InternalServerError,
                        ErrorType.INTERNAL_SERVER_ERROR,
                        result.Message
                    ));
            }

            return StatusCode(
                StatusCodes.Status201Created,
                new ApiCommandDataResponse<FileUploadDto>(
                    result.Message,
                    StatusCodes.Status201Created,
                    result.Data));
        }
    }
}
