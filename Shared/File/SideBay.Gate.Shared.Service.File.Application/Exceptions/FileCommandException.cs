﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Application.Exceptions
{
    public class FileCommandException : Exception
    {
        public FileCommandException(string message) : base(message)
        {

        }
    }
}
