﻿using AutoMapper;
using SideBay.Gate.Shared.Service.File.Application.Command.Create;
using SideBay.Gate.Shared.Service.File.Application.Dtos;
using SideBay.Gate.Shared.Service.File.Infrastructure.Models;
using System;

namespace SideBay.Gate.Shared.Service.File.Application.Mapper
{
    public class CreateFileUploadCommandMapper : Profile
    {
        public CreateFileUploadCommandMapper()
        {
            CreateMap<CreateFileUploadCommand, FileUpload>()
                .ForMember(d => d.Id, s => s.MapFrom(src => Guid.NewGuid()))
                .ForMember(d => d.Content, s => s.MapFrom(src => src.Content))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Path, s => s.MapFrom(src => src.Path))
                .ForMember(d => d.Extension, s => s.MapFrom(src => src.Extension))
                .ForMember(d => d.FileType, s => s.MapFrom(src => src.FileType))
                .ForMember(d => d.CreatedDate, s => s.MapFrom(src => DateTime.UtcNow));


            CreateMap<FileUpload, FileUploadDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.FileName, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Path, s => s.MapFrom(src => src.Path));
                
        }
    }
}
