﻿using MediatR;
using SideBay.Gate.Core.Dtos.Applications;
using SideBay.Gate.Shared.Service.File.Application.Dtos;
using SideBay.Gate.Shared.Service.File.Application.Exceptions;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SideBay.Gate.Shared.Service.File.Application.Command.Create
{
    public class CreateFileUploadCommand : IRequest<IResult<FileUploadDto>>
    {
        public byte[] Content { get; private set; }
        public string Path { get; private set; }
        public string Name { get; private set; }
        public string Extension
        {
            get
            {
                if (string.IsNullOrEmpty(Name))
                    return string.Empty;
                return System.IO.Path.GetExtension(Name);
            }
        }
        public string FileType{ get; private set;}

        public void SetFileName(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new FileCommandException("File name can not be null or empty");

            var extension = System.IO.Path.GetExtension(fileName);
            var fname = System.IO.Path.GetFileNameWithoutExtension(fileName);
            var modifiedFileName = $"{fname}_{DateTime.UtcNow.ToString("yyyyMMddHHmmss")}{extension}";
            this.Name = modifiedFileName;
        }

        public void SetContent(byte[] content)
        {
            this.Content = content;
        }

        public void SetPath(string path)
        {
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException("System Path config is not exist");
            }

            this.Path = System.IO.Path.Combine(path, this.Name);
        }

        public void SetFileType(string fileType)
        {
            this.FileType = fileType;
        }

        private string GetHashFileName(string fileName, string extension)
        {
            var hashFileName = BitConverter.ToString(new SHA512CryptoServiceProvider()
                .ComputeHash(Encoding.Default.GetBytes(fileName)))
                .Replace("-", string.Empty).ToUpper();
            return $"{hashFileName.ToLower()}{extension}";
        }
    }
}
