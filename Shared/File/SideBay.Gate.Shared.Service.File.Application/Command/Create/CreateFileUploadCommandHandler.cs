﻿using MediatR;
using Microsoft.Extensions.Options;
using SideBay.Gate.Core.Dtos.Applications;
using SideBay.Gate.Shared.Service.File.Application.Dtos;
using SideBay.Gate.Shared.Service.File.Application.Mapper;
using SideBay.Gate.Shared.Service.File.Infrastructure.Configs;
using SideBay.Gate.Shared.Service.File.Infrastructure.Models;
using SideBay.Gate.Shared.Service.File.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Application.Command.Create
{
    public class CreateFileUploadCommandHandler : IRequestHandler<CreateFileUploadCommand, IResult<FileUploadDto>>
    {
        private readonly IFileRepository _fileRepository;

        public CreateFileUploadCommandHandler(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public async Task<IResult<FileUploadDto>> Handle(CreateFileUploadCommand request, CancellationToken cancellationToken)
        {
            var fileUpload = ApplicationMapping.Mapper.Map<FileUpload>(request);

            await this._fileRepository.SaveFileAsync(fileUpload);

            var fileUploadDto = ApplicationMapping.Mapper.Map<FileUploadDto>(fileUpload);

            return await Task.FromResult(Result<FileUploadDto>.SuccessData("Saved file successfully", fileUploadDto));
        }
    }
}
