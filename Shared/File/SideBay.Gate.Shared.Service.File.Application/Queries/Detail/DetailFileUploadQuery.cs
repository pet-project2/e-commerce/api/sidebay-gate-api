﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Application.Queries.Detail
{
    public class DetailFileUploadQuery
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
    }
}
