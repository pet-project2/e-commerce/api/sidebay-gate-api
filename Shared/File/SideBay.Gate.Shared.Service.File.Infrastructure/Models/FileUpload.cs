﻿using System;

namespace SideBay.Gate.Shared.Service.File.Infrastructure.Models
{
    public class FileUpload
    {
        public Guid Id { get; set; }
        public byte[] Content { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }
        public string FileType { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
