﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Infrastructure.Repository
{
    public interface IUnitOfWork
    {
        Task Commit();
    }
}
