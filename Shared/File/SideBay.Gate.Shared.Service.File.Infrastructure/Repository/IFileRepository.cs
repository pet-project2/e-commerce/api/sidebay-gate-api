﻿using SideBay.Gate.Shared.Service.File.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Infrastructure.Repository
{
    public interface IFileRepository
    {
        Task<FileUpload> GetFile(Guid id);
        Task<FileUpload> GetFile(string fileName);
        Task<FileUpload> GetFile(string fileName, string fileType);
        Task SaveFileAsync(FileUpload file);
    }
}
