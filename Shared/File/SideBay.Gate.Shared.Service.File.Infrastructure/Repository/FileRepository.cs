﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SideBay.Gate.Shared.Service.File.Infrastructure.Configs;
using SideBay.Gate.Shared.Service.File.Infrastructure.Models;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SideBay.Gate.Shared.Service.File.Infrastructure.Repository
{
    public class FileRepository : IFileRepository
    {
        private readonly AppDbContext _appDbContext;
        private readonly IUnitOfWork _unitOfWork;
        private ILogger<FileRepository> _logger;

        public FileRepository(AppDbContext appDbContext,
            IUnitOfWork unitOfWork,
            IOptions<AppConfiguration> options,
            ILogger<FileRepository> logger)
        {
            _appDbContext = appDbContext;
            _unitOfWork = unitOfWork;
            this._logger = logger;
        }

        public async Task<FileUpload> GetFile(Guid id)
        {
            var file = await this._appDbContext.FileUploads.FindAsync(id);
            return file;
        }

        public Task<FileUpload> GetFile(string fileName)
        {
            throw new NotImplementedException();
        }

        public Task<FileUpload> GetFile(string fileName, string fileType)
        {
            throw new NotImplementedException();
        }

        public async Task SaveFileAsync(FileUpload file)
        {
            var filePath = file.Path;
            try
            {
                using (var fileStream = new FileStream(filePath, FileMode.CreateNew))
                {
                    MemoryStream ms = new MemoryStream(file.Content);
                    ms.WriteTo(fileStream);
                }

                await this._appDbContext.FileUploads.AddAsync(file);
                await this._unitOfWork.Commit();
            }
            catch(IOException ex)
            {
                this._logger.LogError($"Something happened when created new file cause: \n {ex.Message}");
                this.DeleteFile(filePath);
                throw new Exception(ex.Message);
            }
            catch(SqlException efex)
            {
                this._logger.LogError($"Something happened when insert new file to db: \n {efex.Message}");
                this.DeleteFile(filePath);
                throw new Exception(efex.Message);
            }
        }


        private void DeleteFile(string path)
        {
            if(System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
        }
    }
}
