
Generate ssl key
openssl req -newkey rsa:2048 -nodes -keyout sidebaygate.key -x509 -days 365 -out sidebaygate.cer

Generate pfx file
openssl pkcs12 -export -in sidebaygate.cer -inkey sidebaygate.key -out sidebaygate.pfx

Add Migration DbContext IdentityServer4 PersistedGrantDb
Add-Migration InitialIdentityServerPersistedGrantDbMigration -Context PersistedGrantDbContext -o Data/Migrations/IdentityServer/PersistedGrantDb


Add Migration DbContext IdentityServer4 ConfigurationDbContext 

Add-Migration InitialIdentityServerPersistedGrantDbMigration -Context ConfigurationDbContext -o Data/Migrations/IdentityServer/ConfigurationDb

Update database PersistedGrantDb
Update-Database -c PersistedGrantDbContext

Update database ConfigurationDbContext
Update-Database -c ConfigurationDbContext


Add Migration Account Dbcontext
Add-Migration initdb -o Data/Migrations

#Redis
docker run --restart=always -d --name redis-stack-server -p 6379:6379 redis:latest