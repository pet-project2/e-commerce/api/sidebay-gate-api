﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Configs
{
    internal class ImageConfiguration : IEntityTypeConfiguration<Models.Image>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Models.Image> builder)
        {
            builder.ToTable("Image");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name);
            builder.Property(x => x.Path);
            builder.Property(x => x.IsMainAvatar);
            builder.Property(x => x.CreatedAt);
            builder.Ignore(x => x.DomainEvents);
        }
    }
}
