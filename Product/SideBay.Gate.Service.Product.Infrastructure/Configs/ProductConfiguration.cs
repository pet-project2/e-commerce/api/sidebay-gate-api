﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SideBay.Gate.Service.Product.Infrastructure.Configs
{
    public class ProductConfiguration : IEntityTypeConfiguration<Models.Product>
    {
        public void Configure(EntityTypeBuilder<Models.Product> builder)
        {
            builder.ToTable("Product");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Sku);
            builder.Property(x => x.Name);
            builder.Property(x => x.Description);
            builder.Property(x => x.Summary);

            builder.Property(x => x.Price)
                .HasColumnType("decimal(18,3)");

            builder.Property(x => x.Discount)
                .HasColumnType("decimal(18,3)");
            builder.Property(x => x.CurrencyCode);

            builder.Property(x => x.CreatedAt);
            builder.Property(x => x.UpdatedAt);
            builder.Property(x => x.PublishedAt);
            builder.Ignore(x => x.DomainEvents);


            builder.HasMany(x => x.Gallery)
                .WithOne(x => x.Product)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
