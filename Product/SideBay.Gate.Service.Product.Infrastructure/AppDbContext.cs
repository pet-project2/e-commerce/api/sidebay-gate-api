﻿using Microsoft.EntityFrameworkCore;
using SideBay.Gate.Service.Product.Infrastructure.Configs;

namespace SideBay.Gate.Service.Product.Infrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new ImageConfiguration());
        }

        public virtual DbSet<Models.Product> Products { get; set; }
        public virtual DbSet<Models.Image> Images { get; set; }
    }
}
