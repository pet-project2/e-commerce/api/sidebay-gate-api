﻿using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository
{
    public interface IReadRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);
        Task<IEnumerable<T>> GetListAsync(int offset, int limit, CancellationToken cancellationToken = default);
    }
}
