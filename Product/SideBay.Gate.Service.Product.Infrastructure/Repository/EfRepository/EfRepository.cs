﻿using Microsoft.EntityFrameworkCore;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ImageRepositoryEf;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ProductRepositoryEf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository
{
    public class EfRepository : IEfRepository<Models.Product>
    {
        private readonly IProductEfRepository<Models.Product> _productRepository;
        private readonly IImageEfRepository<Models.Image> _imageRepository;
        private readonly IUnitOfWork _unitOfWork;
        public EfRepository(IProductEfRepository<Models.Product> productRepository,
            IImageEfRepository<Models.Image> imageRepository, IUnitOfWork unitOfWork)
        {
            this._productRepository = productRepository;
            this._imageRepository = imageRepository;
            this._unitOfWork = unitOfWork;
        }

        public Task AddAsync(Models.Product entity, CancellationToken cancellationToken = default)
        {
            return this._productRepository.AddProductAsync(entity, cancellationToken);
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var product = await this.GetByIdAsync(id, cancellationToken);
            this._productRepository.DeleteProductAsync(product, cancellationToken);
        }

        public Task<Models.Product> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return this._productRepository.GetByProductIdAsync(id, cancellationToken);
        }

        public Task<IEnumerable<Models.Product>> GetListAsync(int offset, int limit, CancellationToken cancellationToken = default)
        {
            return this._productRepository.GetListProductAsync(offset, limit, cancellationToken);
        }

        public async Task UpdateAsync(Models.Product entity, CancellationToken cancellationToken = default)
        {
            var product = await this.GetByIdAsync(entity.Id, cancellationToken);
            if (product == Models.Product.Default)
            {
                string errMessage = "Can not update when couldn't found old product";
                throw new DbUpdateException(errMessage);
            }

            // delete old image
            var listOldImage = product.Gallery.Where(x => entity.Gallery.Any(c => c.Id != x.Id));
            this._imageRepository.DeleteRange(listOldImage);

            // add image to new list
            var listNewImage = entity.Gallery.Where(x => product.Gallery.Any(c => c.Id != x.Id));
            await this._imageRepository.AddRangeAsync(listNewImage);

            this._productRepository.UpdateProduct(product, entity, cancellationToken);
        }
    }
}
