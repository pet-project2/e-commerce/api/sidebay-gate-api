﻿using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ProductRepositoryEf
{
    public interface IProductEfRepository<T> where T : class, IAggregateRoot
    {
        Task<T> GetByProductIdAsync(Guid productId, CancellationToken cancellationToken = default);
        Task<IEnumerable<T>> GetListProductAsync(int offset, int limit, CancellationToken cancellationToken = default);
        Task AddProductAsync(T product, CancellationToken cancellationToken = default);
        void UpdateProduct(T product, CancellationToken cancellationToken = default);
        void UpdateProduct(T oldProduct, T newProduct, CancellationToken cancellationToken = default);
        Task DeleteProductAsync(Guid productId, CancellationToken cancellationToken = default);
        void DeleteProductAsync(T product, CancellationToken cancellationToken = default);
    }
}
