﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ProductRepositoryEf
{
    public class ProductEfRepository : IProductEfRepository<Models.Product>
    {
        private readonly AppDbContext _appDbContext;

        public ProductEfRepository(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public async Task AddProductAsync(Models.Product product, CancellationToken cancellationToken = default)
        {
            await this._appDbContext.Products.AddAsync(product);
        }

        public async Task DeleteProductAsync(Guid productId, CancellationToken cancellationToken = default)
        {
            var product = await this._appDbContext.Products.FirstOrDefaultAsync(x => x.Id == productId);
            if(product != null)
            {
                this.DeleteProductAsync(product, cancellationToken);
            }
        }

        public void DeleteProductAsync(Models.Product product, CancellationToken cancellationToken = default)
        {
            this._appDbContext.Remove(product);
        }

        public Task<Models.Product> GetByProductIdAsync(Guid productId, CancellationToken cancellationToken = default)
        {
            return this._appDbContext.Products
                .AsNoTracking()
                .Include(p => p.Gallery)
                .FirstOrDefaultAsync(x => x.Id == productId);
        }                            

        public async Task<IEnumerable<Models.Product>> GetListProductAsync(int offset, int limit, CancellationToken cancellationToken = default)
        {
            return await this._appDbContext.Products
                .AsNoTracking()
                .Skip(offset)
                .Take(limit)
                .Include(p => p.Gallery)
                .ToListAsync();
        }

        public void UpdateProduct(Models.Product product, CancellationToken cancellationToken = default)
        {
            this._appDbContext.Products.Update(product);
        }

        public void UpdateProduct(Models.Product oldProduct, Models.Product newProduct, CancellationToken cancellationToken = default)
        {
            this._appDbContext.Entry(oldProduct).CurrentValues.SetValues(newProduct);
        }
    }
}
