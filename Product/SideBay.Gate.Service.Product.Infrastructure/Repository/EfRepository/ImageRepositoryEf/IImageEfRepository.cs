﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ImageRepositoryEf
{
    public interface IImageEfRepository<T> where T : class
    {
        Task<T> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);
        Task<IEnumerable<T>> GetListAsync(CancellationToken cancellationToken = default);
        Task AddAsync(T image, CancellationToken cancellationToken = default);
        Task AddRangeAsync(IEnumerable<T> images, CancellationToken cancellationToken = default);
        Task UpdateAsync(T image, CancellationToken cancellationToken = default);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);
        void Delete(T image);
        void DeleteRange(IEnumerable<T> images);
    }
}
