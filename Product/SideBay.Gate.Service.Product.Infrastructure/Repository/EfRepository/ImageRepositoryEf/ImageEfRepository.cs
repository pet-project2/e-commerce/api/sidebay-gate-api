﻿using Microsoft.EntityFrameworkCore;
using SideBay.Gate.Service.Product.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ImageRepositoryEf
{
    public class ImageEfRepository : IImageEfRepository<Image>
    {
        private readonly AppDbContext _appDbContext;

        public ImageEfRepository(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public async Task AddAsync(Image image, CancellationToken cancellationToken = default)
        {
            await this._appDbContext.AddAsync(image, cancellationToken);
            this._appDbContext.Entry(image).State = EntityState.Added;
        }

        public async Task AddRangeAsync(IEnumerable<Image> images, CancellationToken cancellationToken = default)
        {
            foreach(var image in images)
            {
                await this.AddAsync(image, cancellationToken);
            }
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            if(id != Guid.Empty)
            {
                var image = await this.GetByIdAsync(id, cancellationToken);
                if (image != Image.Default)
                {
                    this.Delete(image);
                }
            }
        }

        public void Delete(Image image)
        {
            if(image != null && image != Image.Default)
            {
                this._appDbContext.Remove(image);
            }
        }

        public void DeleteRange(IEnumerable<Image> images)
        {
            if(images != null && images.Any())
            {
                this._appDbContext.RemoveRange(images);
            }
        }

        public async Task<Image> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var image = await this._appDbContext.Images
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
            return image ?? Image.Default;
        }

        public async Task<IEnumerable<Image>> GetListAsync(CancellationToken cancellationToken = default)
        {
            return await this._appDbContext.Images
                .AsNoTracking()
                .ToListAsync(cancellationToken);
        }

        public Task UpdateAsync(Image image, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
