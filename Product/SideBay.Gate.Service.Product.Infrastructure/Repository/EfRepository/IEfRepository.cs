﻿using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository
{
    public interface IEfRepository<T> : IReadRepository<T>, 
        IWriteRepository<T> where T : class, IAggregateRoot
    {

    }
}
