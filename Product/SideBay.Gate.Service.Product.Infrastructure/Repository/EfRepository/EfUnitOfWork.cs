﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<EfUnitOfWork> _logger;

        public EfUnitOfWork(AppDbContext appDbContext, ILogger<EfUnitOfWork> logger)
        {
            this._appDbContext = appDbContext;
            this._logger = logger;
        }

        public Task Commit()
        {
            try
            {
                return this._appDbContext.SaveChangesAsync();
            }
            catch (SqlException sqlEx)
            {
                this._logger.LogError($"Something happened when excute on db: {sqlEx.Message}");
                throw new Exception(sqlEx.Message);
            }
        }
    }
}
