﻿using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository
{
    public class ReadRepository : IReadRepository<Models.Product>
    {
        private readonly IEfRepository<Models.Product> _efRepository;

        public ReadRepository(IEfRepository<Models.Product> efRepository)
        {
            this._efRepository = efRepository;
        }

        public Task<Models.Product> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return this._efRepository.GetByIdAsync(id, cancellationToken);
        }

        public Task<IEnumerable<Models.Product>> GetListAsync(int offset, int limit, CancellationToken cancellationToken = default)
        {
            return this._efRepository.GetListAsync(offset, limit, cancellationToken);  
        }
    }
}
