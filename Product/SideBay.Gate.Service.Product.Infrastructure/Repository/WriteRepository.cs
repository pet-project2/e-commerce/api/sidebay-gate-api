﻿using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Repository
{
    public class WriteRepository : IWriteRepository<Models.Product>
    {
        private readonly IEfRepository<Models.Product> _efRepository;

        public WriteRepository(IEfRepository<Models.Product> efRepository)
        {
            this._efRepository = efRepository;
        }

        public Task AddAsync(Models.Product entity, CancellationToken cancellationToken = default)
        {
           return this._efRepository.AddAsync(entity, cancellationToken);
        }

        public Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
           return this._efRepository.DeleteAsync(id, cancellationToken);
        }

        public Task UpdateAsync(Models.Product entity, CancellationToken cancellationToken = default)
        {
           return this._efRepository.UpdateAsync(entity, cancellationToken);
        }
    }
}
