﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SideBay.Gate.Service.Product.Infrastructure.Migrations
{
    public partial class addImageName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Image",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Image");
        }
    }
}
