﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SideBay.Gate.Service.Product.Infrastructure.Migrations
{
    public partial class updateProductProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MainAvatar",
                table: "Product",
                newName: "Sku");

            migrationBuilder.AddColumn<string>(
                name: "CurrencyCode",
                table: "Product",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsMainAvatar",
                table: "Image",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrencyCode",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "IsMainAvatar",
                table: "Image");

            migrationBuilder.RenameColumn(
                name: "Sku",
                table: "Product",
                newName: "MainAvatar");
        }
    }
}
