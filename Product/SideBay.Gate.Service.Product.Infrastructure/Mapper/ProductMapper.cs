﻿using AutoMapper;
using SideBay.Gate.Service.Product.Domain.Aggregate;
using SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Mapper
{
    public class ProductMapper : Profile
    {
        public ProductMapper()
        {
            CreateMap<ProductAggregate, Models.Product>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Sku, s => s.MapFrom(src => src.Sku))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Description, s => s.MapFrom(src => src.Description))
                .ForMember(d => d.Price, s => s.MapFrom(src => src.Price.Amount))
                .ForMember(d => d.CurrencyCode, s => s.MapFrom(src => src.Price.Currency.CurrencyCode))
                .ForMember(d => d.Discount, s => s.MapFrom(src => src.Discount))
                .ForMember(d => d.CreatedAt, s => s.MapFrom(src => src.CreatedAt))
                .ForMember(d => d.UpdatedAt, s => s.MapFrom(src => src.UpdatedAt))
                .ForMember(d => d.PublishedAt, s => s.MapFrom(src => src.PublishedAt))
                .ForMember(d => d.Gallery, s => s.MapFrom(src => src.Gallery))
                .ForMember(d => d.DomainEvents, s => s.MapFrom((src, des, result) =>
                {
                    result = src.DomainEvents;
                    src.ClearDomainEvents();
                    return result;
                })
            );

            CreateMap<Models.Product, ProductAggregate>()
                .ConstructUsing((source, destination) =>
                {
                    var productAgg = new ProductAggregate(
                        source.Id,
                        source.Name,
                        source.Description,
                        source.Summary,
                        new Money(source.Price, source.CurrencyCode),
                        source.Discount
                    );

                    productAgg.UpdateCreatedDate(source.CreatedAt);
                    productAgg.UpdateUpdatedDate(source.UpdatedAt);
                    productAgg.UpdatePublishDate(source.PublishedAt);
                    return productAgg;
                })
                .ForAllMembers(s => s.Ignore());
        }
    }
}
