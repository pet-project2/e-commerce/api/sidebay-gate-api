﻿using AutoMapper;
using SideBay.Gate.Service.Product.Domain.Aggregate.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Mapper
{
    public class ImageMapper : Profile
    {
        public ImageMapper()
        {
            CreateMap<Image, Models.Image>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Path, s => s.MapFrom(src => src.Path))
                .ForMember(d => d.IsMainAvatar, s => s.MapFrom(src => src.IsMainAvatar))
                .ForMember(d => d.CreatedAt, s => s.MapFrom(src => src.CreatedAt))
                .ForMember(d => d.ProductId, s => s.Ignore());
        }
    }
}
