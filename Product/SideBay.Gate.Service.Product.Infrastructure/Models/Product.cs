﻿using SideBay.Gate.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Models
{
    public class Product : BaseModel<Guid>, IAggregateRoot
    {
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public decimal Price { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Discount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime PublishedAt { get; set; }
        public ICollection<Models.Image> Gallery { get; set; }

        public static Product Default
        {
            get
            {
                return new Product();
            }
        }
    }
}
