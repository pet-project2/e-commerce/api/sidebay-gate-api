﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Models
{
    public class Image : BaseModel<Guid>
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsMainAvatar { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public static Image Default
        {
            get
            {
                return new Image();
            }
        }

    }
}
