﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Infrastructure.Models
{
    public class BaseModel<TId>
    {
        private int? _requestedHashCode;
        public TId Id { get; set; }
        public IReadOnlyCollection<INotification> DomainEvents { get; set; }
        public bool IsTransient()
        {
            return object.Equals(Id, default(TId));
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is BaseModel<TId>))
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            if (this.GetType() != obj.GetType())
                return false;

            BaseModel<TId> item = obj as BaseModel<TId>;

            if (item.IsTransient() || this.IsTransient())
                return false;

            return object.Equals(item.Id, this.Id);
        }

        public static bool operator == (BaseModel<TId> left, BaseModel<TId> right)
        {
            if (left is null && right is null)
                return true;
            if (left is null || right is null)
                return false;

            return left.Equals(right);
        }

        public static bool operator != (BaseModel<TId> left, BaseModel<TId> right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            if (!IsTransient())
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = this.Id.GetHashCode() ^ 31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)

                return _requestedHashCode.Value;
            }
            else
                return base.GetHashCode();
        }
    }
}
