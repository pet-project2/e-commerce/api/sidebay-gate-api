﻿using Microsoft.AspNetCore.Hosting;
using SideBay.Gate.Service.Product.Api.Constants;

namespace SideBay.Gate.Service.Product.Api.Extensions
{
    public static class WebHostEnvironmentExtensions
    {
        public static bool IsDevelopmenmt(this IWebHostEnvironment env)
        {
            return (env.EnvironmentName.ToLower() == EnviromentConst.DEVELOPMENT.ToLower());
        }
    }
}
