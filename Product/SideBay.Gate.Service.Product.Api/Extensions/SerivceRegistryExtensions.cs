﻿using Microsoft.Extensions.DependencyInjection;
using SideBay.Gate.Service.Product.Infrastructure.Repository;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ImageRepositoryEf;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository.ProductRepositoryEf;

namespace SideBay.Gate.Service.Product.Api.Extensions
{
    public static class SerivceRegistryExtensions
    {
        public static void RegistryService(this IServiceCollection services)
        {
            services.AddScoped<IImageEfRepository<Infrastructure.Models.Image>, ImageEfRepository>();
            services.AddScoped<IProductEfRepository<Infrastructure.Models.Product>, ProductEfRepository>();
            services.AddScoped<IEfRepository<Infrastructure.Models.Product>, EfRepository>();
            services.AddScoped<IUnitOfWork, EfUnitOfWork>();
            services.AddScoped<IReadRepository<Infrastructure.Models.Product>, ReadRepository>();
            services.AddScoped<IWriteRepository<Infrastructure.Models.Product>, WriteRepository>();
        }
    }
}
