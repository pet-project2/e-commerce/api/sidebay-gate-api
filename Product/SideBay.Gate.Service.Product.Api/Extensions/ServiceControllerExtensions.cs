﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Converters;
using SideBay.Gate.Service.Product.Api.Filters;
using SideBay.Gate.Service.Product.Application.Validations;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SideBay.Gate.Service.Product.Api.Extensions
{
    public static class ServiceControllerExtensions
    {
        public static void RegistryController(this IServiceCollection services)
        {
            // validation
            services.AddControllers().ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            })
            .AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                opts.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(new ValidationFilter());
            })
            .AddFluentValidation(options =>
            {
                options.RegisterValidatorsFromAssemblyContaining(typeof(CreateProductCommandValidation));
            });
        }   
    }
}
