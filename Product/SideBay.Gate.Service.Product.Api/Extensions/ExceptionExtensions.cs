﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SideBay.Gate.Service.Product.Api.Dtos;
using SideBay.Gate.Utilities.Enumes;
using System.Net;

namespace SideBay.Gate.Service.Product.Api.Extensions
{
    public static class ExceptionExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogger logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    var statusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.StatusCode = statusCode;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        logger.LogError($"Something went wrong: {contextFeature.Error}");

                        await context.Response.WriteAsync(new ApiErrorResponse<string>(
                            "Internal Server Error",
                            statusCode,
                            ErrorType.INTERNAL_SERVER_ERROR,
                            contextFeature.Error.Message)
                            .ToString());
                    }
                });
            });
        }
    }
}
