﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace SideBay.Gate.Service.Product.Api.Extensions
{
    public static class ThirdPartyRegistryExtensions
    {
        public static void RegistryThirdParty(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SideBay.Gate.Service.Product.Api", Version = "v1" });
            });
        }
    }
}
