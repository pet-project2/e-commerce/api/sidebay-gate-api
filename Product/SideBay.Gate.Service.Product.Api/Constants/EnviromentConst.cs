﻿namespace SideBay.Gate.Service.Product.Api.Constants
{
    public struct EnviromentConst
    {
        public const string INTEGRATION_TEST = "Integration-Test";
        public const string DEVELOPMENT = "Development";
    }
}
