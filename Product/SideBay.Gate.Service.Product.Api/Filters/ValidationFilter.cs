﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SideBay.Gate.Service.Product.Api.Dtos;
using SideBay.Gate.Utilities.Enumes;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SideBay.Gate.Service.Product.Api.Filters
{
    public class ValidationFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                IEnumerable<string> allErrors = context.ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(x => x.ErrorMessage);
                var apiErrResponse = new ApiErrorResponse<IEnumerable<string>>("Model is not valid",
                    (int)HttpStatusCode.BadRequest,
                    ErrorType.MODEL_INVALID,
                    allErrors);
                context.Result = new BadRequestObjectResult(apiErrResponse);
            }
        }
    }
}
