﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SideBay.Gate.Service.Product.Api.Dtos;
using SideBay.Gate.Service.Product.Application.Command.Create;
using SideBay.Gate.Service.Product.Application.Dtos;
using SideBay.Gate.Service.Product.Application.Queries.List;
using SideBay.Gate.Utilities.Enumes;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpPost]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ApiCommandResponse), StatusCodes.Status201Created)]
        public async Task<IActionResult> CreateProductAsync([FromBody]CreateProductCommand command)
        {
            if(command is null)
            {
                return BadRequest(new ApiErrorResponse<string>
                    (
                        "Product 's command model is null",
                        StatusCodes.Status400BadRequest,
                        ErrorType.MODEL_INVALID,
                        "Command model is null"
                    ));
            }

            var result = await this._mediator.Send(command);

            if (!result.IsSuccess)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new ApiErrorResponse<string>
                    (
                        "Somthing happend when create new product",
                        StatusCodes.Status500InternalServerError,
                        ErrorType.INTERNAL_SERVER_ERROR,
                        result.Message
                    ));
            }

            return StatusCode(
                StatusCodes.Status201Created,
                new ApiCommandResponse(
                result.Message,
                StatusCodes.Status201Created));
        }

        [HttpGet]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiErrorResponse<string>), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ApiQueryResponse<ListProductDto>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetListProductAsync([FromQuery] int pageNumber = 1, [FromQuery] int pageSize = 10)
        {
            var query = new ListProductQuery(pageNumber, pageSize);
            var result = await this._mediator.Send(query);

            if (!result.IsSuccess)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new ApiErrorResponse<string>
                    (
                        "Something happend when get products",
                        StatusCodes.Status500InternalServerError,
                        ErrorType.INTERNAL_SERVER_ERROR,
                        result.Message
                    ));
            }

            return Ok(
                new ApiQueryResponse<ListProductDto>(
                    result.Message,
                    StatusCodes.Status200OK,
                    result.Data
                )
            );
        }
    }
}
