﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Api.Dtos
{
    public class ApiCommandResponse : ApiBaseResponse
    {
        public ApiCommandResponse(string message, int statusCode) 
            : base(true, message, statusCode)
        {

        }
    }
}
