﻿using AutoMapper;
using SideBay.Gate.Service.Product.Application.Dtos;
using SideBay.Gate.Service.Product.Domain.Aggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Mapper
{
    public class ProductDtoMapper : Profile
    {
        public ProductDtoMapper()
        {
            CreateMap<ProductAggregate, ProductDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Sku, s => s.MapFrom(src => src.Sku))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Description, s => s.MapFrom(src => src.Description))
                .ForMember(d => d.Summary, s => s.MapFrom(src => src.Summary))
                .ForMember(d => d.Price, s => s.MapFrom(src => src.Price.Amount))
                .ForMember(d => d.ActualPrice, s => s.MapFrom(src => src.ActualPrice.Amount))
                .ForMember(d => d.CurrencyCode, s => s.MapFrom(src => src.Price.Currency.CurrencyCode))
                .ForMember(d => d.CreatedAt, s => s.MapFrom(src => src.CreatedAt))
                .ForMember(d => d.UpdatedAt, s => s.MapFrom(src => src.UpdatedAt))
                .ForMember(d => d.PublishedAt, s => s.MapFrom(src => src.PublishedAt))
                .ForMember(d => d.Gallery, s => s.MapFrom(
                    src => ApplicationMapping.Mapper.Map<IEnumerable<ImageDto>>(src.Gallery)));
        }
    }
}
