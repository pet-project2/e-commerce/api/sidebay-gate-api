﻿using AutoMapper;
using SideBay.Gate.Service.Product.Application.Dtos;
using SideBay.Gate.Service.Product.Domain.Aggregate.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Mapper
{
    public class ImageDtoMapper : Profile
    {
        public ImageDtoMapper()
        {
            CreateMap<Image, ImageDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name))
                .ForMember(d => d.Path, s => s.MapFrom(src => src.Path))
                .ForMember(d => d.IsMainAvatar, s => s.MapFrom(src => src.IsMainAvatar))
                .ForAllMembers(d => d.Ignore());
        }
    }
}
