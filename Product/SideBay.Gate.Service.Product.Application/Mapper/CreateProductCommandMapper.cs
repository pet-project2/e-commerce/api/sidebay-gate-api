﻿using AutoMapper;
using SideBay.Gate.Service.Product.Application.Command.Create;
using SideBay.Gate.Service.Product.Domain.Aggregate;
using SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Mapper
{
    public class CreateProductCommandMapper : Profile
    {
        public CreateProductCommandMapper()
        {
            CreateMap<CreateProductCommand, ProductAggregate>()
                .ConstructUsing(x => new ProductAggregate(
                    Guid.NewGuid(),
                    x.Name,
                    x.Description,
                    x.Summary,
                    new Money(x.Price, x.CurrencyCode),
                    x.Discount
            ))
                .ForAllMembers(s => s.Ignore());
        }
    }
}
