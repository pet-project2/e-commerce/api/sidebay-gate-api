﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Dtos
{
    public interface IResult<T>
    {
        public bool IsSuccess { get; }
        public string Message { get; }
        Failure Failure { get; }
        T Data { get; }
    }
}
