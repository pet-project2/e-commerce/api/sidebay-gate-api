﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Dtos
{
    public class ListProductDto
    {
        public IEnumerable<ProductDto> Products { get; set; }
        public Paging Paging { get; set; }
    }
}
