﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Dtos
{
    public class Failure
    {
        public string ErrorType { get; private set; }
        public string Reason { get; private set; }
        public Failure(string errorType, string reason)
        {
            this.ErrorType = errorType;
            this.Reason = reason;
        }
    }
}
