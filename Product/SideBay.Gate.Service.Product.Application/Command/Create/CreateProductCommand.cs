﻿using MediatR;
using SideBay.Gate.Service.Product.Application.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Command.Create
{
    public class CreateProductCommand : IRequest<IResult<Unit>>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public decimal Price { get; set; }
        public string CurrencyCode { get; set; } = "VND";
        public decimal Discount { get; set; }
        public IEnumerable<string> Gallery { get; set; }
    }
}
