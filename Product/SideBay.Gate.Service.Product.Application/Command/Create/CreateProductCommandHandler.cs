﻿using MediatR;
using SideBay.Gate.Service.Product.Application.Dtos;
using SideBay.Gate.Service.Product.Application.Mapper;
using SideBay.Gate.Service.Product.Domain.Aggregate;
using SideBay.Gate.Service.Product.Infrastructure.Mapper;
using SideBay.Gate.Service.Product.Infrastructure.Repository;
using SideBay.Gate.Service.Product.Infrastructure.Repository.EfRepository;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Command.Create
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, IResult<Unit>>
    {
        private readonly IWriteRepository<Infrastructure.Models.Product> _writeRepository;
        private readonly IUnitOfWork _unitOfWork;
        public CreateProductCommandHandler(IWriteRepository<Infrastructure.Models.Product> writeRepository, IUnitOfWork unitOfWork)
        {
            this._writeRepository = writeRepository;
            this._unitOfWork = unitOfWork;
        }
        public async Task<IResult<Unit>> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var productAgg = ApplicationMapping.Mapper.Map<ProductAggregate>(request);
            var dateTimeNow = DateTime.UtcNow;
            productAgg.UpdateCreatedDate(dateTimeNow);
            productAgg.UpdateUpdatedDate(dateTimeNow);

            // TODO - add domain event

            var product = InfrasMapping.Mapper.Map<Infrastructure.Models.Product>(productAgg);
            await this._writeRepository.AddAsync(product);
            await this._unitOfWork.Commit();

            var result = Result<Unit>.Success($"Create new product successfully");

            return result;
        }
    }
}
