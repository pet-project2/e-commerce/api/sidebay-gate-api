﻿using FluentValidation;
using SideBay.Gate.Service.Product.Application.Command.Create;

namespace SideBay.Gate.Service.Product.Application.Validations
{
    public class CreateProductCommandValidation : AbstractValidator<CreateProductCommand>
    {
        public CreateProductCommandValidation()
        {
            RuleFor(c => c.Name)
                .NotNull().WithMessage("Product's name can not be null")
                .NotEmpty().WithMessage("Product's name can not be empty")
                .MaximumLength(200).WithMessage("Product's length is only maximum 200 charaters");

            RuleFor(c => c.Description)
                .NotNull().WithMessage("Product's description can not be null")
                .NotEmpty().WithMessage("Product's description can not be empty");

            RuleFor(c => c.Summary)
                .NotNull().WithMessage("Product's summary can not be null")
                .NotEmpty().WithMessage("Product's summary can not be empty");

            RuleFor(c => c.Price)
                .GreaterThanOrEqualTo(0).WithMessage("Product's price can not be negative");

            RuleFor(c => c.CurrencyCode)
                .NotNull().WithMessage("Product's currencyCode can not be null")
                .NotEmpty().WithMessage("Product's currencyCode can not be empty");

            RuleFor(c => c.Discount)
                .GreaterThanOrEqualTo(0).WithMessage("Product's discount can not be negative");
        }
    }
}
