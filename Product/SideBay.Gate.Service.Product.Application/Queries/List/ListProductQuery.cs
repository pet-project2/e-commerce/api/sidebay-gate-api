﻿using MediatR;
using SideBay.Gate.Service.Product.Application.Dtos;

namespace SideBay.Gate.Service.Product.Application.Queries.List
{
    public class ListProductQuery : IRequest<IResult<ListProductDto>>
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }

        public ListProductQuery(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 || pageSize <= 0 ? 10 : pageSize;
        }
    }
}
