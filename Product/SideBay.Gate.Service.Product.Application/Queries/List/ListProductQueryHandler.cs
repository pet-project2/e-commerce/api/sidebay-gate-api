﻿using MediatR;
using SideBay.Gate.Service.Product.Application.Dtos;
using SideBay.Gate.Service.Product.Application.Mapper;
using SideBay.Gate.Service.Product.Domain.Aggregate;
using SideBay.Gate.Service.Product.Infrastructure.Mapper;
using SideBay.Gate.Service.Product.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Application.Queries.List
{
    public class ListProductQueryHandler : IRequestHandler<ListProductQuery, IResult<ListProductDto>>
    {
        private readonly IReadRepository<Infrastructure.Models.Product> _readRepository;
        public ListProductQueryHandler(IReadRepository<Infrastructure.Models.Product> readRepository)
        {
            _readRepository = readRepository;
        }

        public async Task<IResult<ListProductDto>> Handle(ListProductQuery request, CancellationToken cancellationToken)
        {

            var limit = request.PageSize;
            var offset = limit * (request.PageNumber - 1);
            var pageNumber = request.PageNumber;
            var pageSize = request.PageSize;

            var products = await this._readRepository.GetListAsync(offset, limit, cancellationToken);

            var productAggs = InfrasMapping.Mapper.Map<IEnumerable<ProductAggregate>>(products);
            var productDtos = ApplicationMapping.Mapper.Map<IEnumerable<ProductAggregate>, IEnumerable<ProductDto>>(productAggs);
            var totalItems = productDtos.Count();

            var paging = Paging.GetPaging(pageNumber, pageSize, totalItems);
            var result = Result<ListProductDto>.SuccessData("Get products sucessfully",
                    new ListProductDto()
                    {
                        Paging = paging,
                        Products = productDtos,
                    }

                );

            return result;

        }
    }
}
