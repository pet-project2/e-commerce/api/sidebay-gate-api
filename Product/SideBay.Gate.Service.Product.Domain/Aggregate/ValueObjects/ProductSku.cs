﻿using SideBay.Gate.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects
{
    public class ProductSku : ValueObject
    {
        private string _value { get; }
        public ProductSku(string sku)
        {
            this._value = sku;
        }
         
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}
