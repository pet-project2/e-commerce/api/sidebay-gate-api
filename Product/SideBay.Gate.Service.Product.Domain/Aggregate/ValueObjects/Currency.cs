﻿using SideBay.Gate.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects
{
    public class Currency : ValueObject
    {
        public string CurrencyCode { get; private set; }
        public bool InUse { get; private set; }
        public int DecimalPlaces { get; private set; }
        public static Currency None => new Currency(string.Empty, false, 0);
        public Currency(string currencyCode, bool inUse, int decimalPlace)
        {
            this.CurrencyCode = currencyCode;
            this.InUse = inUse;
            this.DecimalPlaces = decimalPlace;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return CurrencyCode;
            yield return InUse;
            yield return DecimalPlaces;
        }
    }
}
