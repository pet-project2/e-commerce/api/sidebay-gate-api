﻿using SideBay.Gate.Core.Entities;
using SideBay.Gate.Service.Product.Domain.Exceptions;
using SideBay.Gate.Service.Product.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects
{
    public class Money : ValueObject
    {
        public decimal Amount { get; }
        public Currency Currency { get;}

        public Money(decimal amount, string currencyCode)
        {
            if (string.IsNullOrEmpty(currencyCode))
                throw new ArgumentNullException(
                    nameof(currencyCode), "Currency code must be specified"
                );

            var currency = CurrencyLookup.FindCurrency(currencyCode);

            if (!currency.InUse)
                throw new ArgumentException($"Currency {currencyCode} is not valid");

            if (decimal.Round(amount, currency.DecimalPlaces) != amount)
                throw new ArgumentOutOfRangeException(
                    nameof(amount),
                    $"Amount in {currencyCode} cannot have more than {currency.DecimalPlaces} decimals"
                );

            this.Amount = amount;
            this.Currency = currency;
        }

        public Money(decimal amount, Currency currency)
        {
            this.Amount = amount;
            this.Currency = currency;
        }

        public Money Add(Money money)
        {
            if(this.Currency != money.Currency)
                throw new CurrencyMismatchException(
                    "Cannot sum amounts with different currencies"
                );

            return new Money(this.Amount + money.Amount, this.Currency);
        }

        private Money Subtract(Money money)
        {
            if (this.Currency != money.Currency)
                throw new CurrencyMismatchException(
                    "Cannot subtract amounts with different currencies"
                );
            return new Money(this.Amount - money.Amount, this.Currency);
        }

        public static Money operator + (Money left, Money right) => left.Add(right);

        public static Money operator - (Money left, Money right) => left.Subtract(right);

        public override string ToString()
        {
            return $"{Currency.CurrencyCode} {Amount}";
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Amount;
            yield return Currency;
        }
    }
}
