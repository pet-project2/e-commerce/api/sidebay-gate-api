﻿using SideBay.Gate.Core.Entities;
using SideBay.Gate.Core.Interface;
using SideBay.Gate.Service.Product.Domain.Aggregate.Entities;
using SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects;
using System;
using System.Collections.Generic;

namespace SideBay.Gate.Service.Product.Domain.Aggregate
{
    public class ProductAggregate : Entity<Guid>, IAggregateRoot
    {
        private List<Image> _listImage;
        public ProductSku Sku { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Summary { get; private set; }
        public Money Price { get; private set; }
        public decimal Discount { get; private set; }
        public Money ActualPrice
        {
            get
            {
                var price = this.Price.Amount - this.Price.Amount * (this.Discount / 100);
                return new Money(price, Price.Currency);
            }
        }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public DateTime PublishedAt { get; private set; }
        public IReadOnlyCollection<Image> Gallery => _listImage.AsReadOnly();

        public ProductAggregate(Guid Id) : base(Id)
        {
            this._listImage = new List<Image>();
        }

        public ProductAggregate(Guid productId, string name,
            string description, string summary,
            Money price, decimal discount) : this(productId)
        {
            this.Name = name;
            this.Description = description;
            this.Price = price;
            this.Discount = discount;
            this.Summary = summary;

        }

        public void UpdateCreatedDate(DateTime dateTime)
        {
            this.CreatedAt = dateTime;
        }

        public void UpdateUpdatedDate(DateTime dateTime)
        {
            this.UpdatedAt = dateTime;
        }

        public void UpdatePublishDate(DateTime dateTime)
        {
            this.PublishedAt = dateTime;
        }

        public void AddGallery(IEnumerable<string> paths)
        {
            foreach (var path in paths)
            {
                _listImage.Add(new Image(Guid.NewGuid(), path));
            }
        }
    }
}
