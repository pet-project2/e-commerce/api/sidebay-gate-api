﻿using SideBay.Gate.Core.Entities;
using System;

namespace SideBay.Gate.Service.Product.Domain.Aggregate.Entities
{
    public class Image : Entity<Guid>
    {
        public string Name { get; set; }
        public string Path { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public bool IsMainAvatar { get; private set; }
        public Image(Guid Id) : base(Id)
        {

        }

        public Image(Guid id, string path) : this(id)
        {
            this.Path = path;
            this.CreatedAt = DateTime.Now.ToUniversalTime();
        }

        public void SetMainAvatar()
        {
            this.IsMainAvatar = true;
        }
    }
}
