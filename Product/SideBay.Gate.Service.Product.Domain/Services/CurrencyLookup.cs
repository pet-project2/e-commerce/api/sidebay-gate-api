﻿using SideBay.Gate.Service.Product.Domain.Aggregate.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Product.Domain.Services
{
    public static class CurrencyLookup
    {
        static readonly IEnumerable<Currency> _currencies =
            new[]
            {
                new Currency("EUR", true, 2),
                new Currency("USD", true, 2),
                new Currency("JPY", true, 2),
                new Currency("DEM", true, 2),
                new Currency("VND", true, 2)
            };

        public static Currency FindCurrency(string currencyCode)
        {
            var currency = _currencies.FirstOrDefault(x => x.CurrencyCode == currencyCode);
            return currency ?? Currency.None;
        }
    }
}
