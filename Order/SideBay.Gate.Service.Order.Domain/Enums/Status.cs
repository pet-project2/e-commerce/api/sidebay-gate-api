﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Order.Domain.Enums
{
    public enum Status
    {
        [Description("Cancelled Order")]
        CANCELLED = 0,

        [Description("Waiting for confirm or payment")]
        WAITINGCONFIRM = 1,

        [Description("Packing product")]
        PACKING = 2,

        [Description("Delivering product")]
        DELIVERING = 3,

        [Description("Arrived success")]
        ARRIVED = 4
    }
}
