﻿using SideBay.Gate.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Order.Domain.Aggregate.Entities
{
    public class Customer : Entity<Guid>
    {
        public string FirstName { get; private set; }
        public string MiddleName { get; private set; }
        public string LastName { get; private set; }
        public string PhoneNumber { get; private set; }
        public string AddressLine1 { get; private set; }
        public string AddressLine2 { get; private set; }
        public string District { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        private Customer(Guid Id) : base(Id)
        {

        }

        private Customer(Guid Id, string firstName, string middleName, string lastName, string phoneNumber, 
            string addressLine1, string addressLine2, string district, string city, string country) : this(Id)
        {
            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
            this.PhoneNumber = phoneNumber;
            this.AddressLine1 = addressLine1;
            this.AddressLine2 = addressLine2;
            this.District = district;
            this.City = city;
            this.Country = country;
        }

        public static Customer Create(Guid Id, string firstName, string middleName,
            string lastName, string phoneNumber,
            string addressLine1, string addressLine2,
            string district, string city, string country)
        {
            return new Customer(Id, firstName, middleName, lastName, 
                phoneNumber, addressLine1, addressLine2, 
                district, city, country);
        }
    }
}
