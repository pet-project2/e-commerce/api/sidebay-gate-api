﻿using SideBay.Gate.Core.Entities;
using SideBay.Gate.Service.Order.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBay.Gate.Service.Order.Domain.Aggregate.ValueObjects
{
    public class OrderStatus : ValueObject
    {
        private Status _value { get; }

        private OrderStatus(Status status)
        {
            this._value = status;
        }

        public static OrderStatus FromEnum(Status status = Status.WAITINGCONFIRM) => new OrderStatus(status);

        public static OrderStatus FromString(string status)
        {
            if (string.IsNullOrEmpty(status))
                return new OrderStatus(Status.WAITINGCONFIRM);

            if(!Enum.IsDefined(typeof(Status), status))
                return new OrderStatus(Status.WAITINGCONFIRM);

            var convertResult =  (Status)Enum.Parse(typeof(Status), status);
            return new OrderStatus(convertResult);
        }


        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}
