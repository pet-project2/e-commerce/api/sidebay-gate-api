﻿using SideBay.Gate.Core.Entities;
using SideBay.Gate.Core.Interface;
using SideBay.Gate.Service.Order.Domain.Aggregate.Entities;
using SideBay.Gate.Service.Order.Domain.Aggregate.ValueObjects;
using System;

namespace SideBay.Gate.Service.Order.Domain.Aggregate
{
    public class OrderAggregate : Entity<Guid>, IAggregateRoot
    {
        public Customer Customer { get; private set; }
        public OrderStatus Status { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdateAt { get; private set; }
        public OrderAggregate(Guid Id) : base(Id)
        {

        }
    }
}
